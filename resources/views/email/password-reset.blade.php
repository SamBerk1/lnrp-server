<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table width="703" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center">
					<img src="{{ URL::asset("/img/logo-black-white.png") }}" style="margin: 40px 0;" />
				</td>
			</tr>
			<tr>
				<td align="center" style="font-size: 18px; color: black; font-family: 'Helvetica Neue', Arial, sans-serif; padding: 10px 15% 0; font-weight: normal;">
					Click the link below to reset your password.<br />If you did not request to reset your password, disregard this email. Nothing will be changed.
				</td>
			</tr>
            <tr>
                <td align="center" style="font-size: 14px; color: black; font-family: 'Helvetica Neue', Arial, sans-serif; padding: 10px 15% 0; font-weight: normal;">
                    <a href="{{ $values['reset_link'] }}">Reset My Password</a>
                </td>
            </tr>
            <tr>
                <td align="center" style="font-size: 14px; color: black; font-family: 'Helvetica Neue', Arial, sans-serif; padding: 10px 15% 0; font-weight: normal;">
					<span style="font-size: 12px; color: #6E7580; font-family: 'Helvetica Neue', Arial, sans-serif;">
						If your email client does not support links or images, copy and paste<br />the following URL in your browser to activate your account:<br />
						{{ $values['reset_link'] }}
					</span>
                </td>
            </tr>
		</table>
	</body>
</html>
