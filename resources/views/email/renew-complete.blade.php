<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table width="703" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center">
					<img src="{{ URL::asset("/img/logo-black-white.png") }}" style="margin: 40px 0;" />
				</td>
			</tr>
			<tr>
				<td align="center" style="font-size: 18px; color: black; font-family: 'Helvetica Neue', Arial, sans-serif; padding: 10px 15% 0; font-weight: normal;">
					Thank you for reactivating your account with Late Night Record Pool. This email is to inform you that your payment has gone through and has been accepted. Your account is now active and you can now log in using the username and password you registered with.<br />
                    <br />
                @if ($values['subscription'] === 'lnrp_monthly')
                    For your reference, $47 is to be deducted every month automatically from your card.
                @elseif ($values['subscription'] === 'lnrp_quarterly')
                    For your reference, $127 is to be deducted every three months automatically from your card.
                @elseif ($values['subscription'] === 'lnrp_halfyear')
                    For your reference, $227 is to be deducted every six months automatically from your card.
                @elseif ($values['subscription'] === 'lnrp_yearly')
                    For your reference, $397 is to be deducted every year automatically from your card.
                @endif
                    <br /><br />
                    It is your responsibility to cancel your account by contacting LNRP staff if you do not want to be automatically charged at the end of your billing cycle.<br />
                    <br />
                    If you have any further questions, please visit the 'Contact Us' page.
				</td>
			</tr>
		</table>
	</body>
</html>
