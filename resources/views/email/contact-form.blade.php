<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<table width="703" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td align="center">
					<img src="{{ URL::asset("/img/logo-black-white.png") }}" style="margin: 40px 0;" />
				</td>
			</tr>
			<tr>
				<td align="center" style="font-size: 18px; color: black; font-family: 'Helvetica Neue', Arial, sans-serif; padding: 10px 15% 0; font-weight: normal;">
					A {{ $values['purpose'] }} Contact Form has been received!
				</td>
			</tr>
            <tr>
                <td align="center" style="font-size: 14px; color: black; font-family: 'Helvetica Neue', Arial, sans-serif; padding: 10px 15% 0; font-weight: normal;">
                    {{ $values['message'] }}
                </td>
            </tr>
            <tr>
                <td align="left" style="font-size: 12px; color: #6E7580; font-family: 'Helvetica Neue', Arial, sans-serif; padding: 30px 15% 0; font-weight: normal;">
                    General Information:<br />
                    From: {{ $values['name'] }}<br />
                    Email: {{ $values['email'] }}<br />
                    Phone: {{ $values['phone'] }}
                </td>
            </tr>
		</table>
	</body>
</html>
