<?php

namespace App;

use App\DownloadedRelease;
use App\DpwnloadedVersion;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;

use Laravel\Cashier\Billable;

class User extends Authenticatable implements JWTSubject
{
    // Trait for using the Cashier package in Laravel
    use Billable;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'iss',
        'iat',
        'exp',
        'nbf',
        'jti',
        'sub',
        'dropbox',
        'versions',
        'releases'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'old_password',
        'remember_token',
    ];


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        $custom = array_where($this->attributes, function($value, $key) {
            return !(in_array($key, $this->guarded) || in_array($key, $this->hidden));
        });

        $custom['dropbox'] = is_null($this->dropbox) ? null : $this->dropbox->token;

        // Populate versions array
        $custom['versions'] = $this->versions->map(function($item, $key) {
            return $item->version;
        });

        // Populate releases array
        $custom['releases'] = $this->releases->map(function($item, $key) {
            return $item->song;
        });

        return $custom;
    }

    public function dropbox()
    {
        return $this->hasOne('App\DropboxToken', 'user');
    }

    public function releases()
    {
        return $this->hasMany('App\DownloadedRelease', 'user');
    }

    public function versions()
    {
        return $this->hasMany('App\DownloadedVersion', 'user');
    }
}
