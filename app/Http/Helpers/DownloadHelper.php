<?php
namespace App\Http\Helpers;

use \App\Code;
use \App\DownloadedRelease;
use \App\DownloadedVersion;
use \App\PreferredVersion;
use \App\Release;
use \App\Version;
use \App\DownloadLog;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class DownloadHelper
{
    public static function generateDownloadCode($user, $release, $version)
    {
        $download_code = new Code();
        $download_code->code = str_random(64);
        $download_code->user = $user;

        if ($version === false) {
            // Requesting zip file release
            $download_code->type = 'release';
            $download_code->id = $release;

            $release = app('db')->select(
                "SELECT id FROM latenight.songs WHERE id = {$release}"
            );

        } else {
            // Requesting specific version file
            $download_code->type = 'version';
            $download_code->id = $version;

            $release = app('db')->select(
                "SELECT id FROM latenight.songs_versions WHERE id = {$version}"
            );
        }

        if (count($release) > 0) {
            $download_code->save();
            #return $download_code->code;
            return $download_code;
        }

        return false;
    }

    public static function generateDownloadFiles($download_code)
    {
        if ($download_code->type === 'version') {
            // Requested code is for a single file

            if (env('APP_ENV') == 'local') {
                // Hard code a response for localized testing
                $file = '/var/drive_b/test_release/Drake - Pop Style (Dirty).mp3';
                $filename = 'Drake - Pop Style (Dirty).mp3';

            } else {
                $file = current(
                    app('db')->select(
                        'SELECT s.artist, s.title, sv.type AS `version`, sv.filename '.
                        'FROM latenight.songs_versions sv '.
                        'INNER JOIN latenight.songs s ON s.id = sv.song '.
                        "WHERE sv.id = {$download_code->id}"
                    )
                );

                if (strpos($file->version, "(") === false) {
                    $version = "(" . $file->version . ")";
                } else {
                    $version_strings = explode("(", $file->version);
                    if (strlen($version_strings[0]) > 0) {
                        $version_strings[0] = "(" . substr($version_strings[0], 0, -1) . ") ";
                    }
                    $version = implode("(", $version_strings);
                }
                $filename = "{$file->artist} - {$file->title} {$version}.mp3";
                $file = $file->filename;
            }

        } else {
            // Requested code is for a full release

            if (env('APP_ENV') == 'local') {
                // Hard code a response for localized testing
                $file = '/var/drive_b/test_release/Drake - Pop Style.zip';
                $filename = 'Drake - Pop Style.zip';

            } else {
                $file = current(
                    app('db')->select(
                        "SELECT artist, title, zip FROM latenight.songs WHERE id = {$download_code->id}"
                    )
                );

                $filename = "{$file->artist} - {$file->title}.zip";
                $file = $file->zip;
            }
        }

        return [
            $file,
            $filename
        ];
    }

    public static function updateDownloadedCounts($user, $type, $id, $ip, $selector)
    {
        // Version information is utilized in a few places
        // If the type is version, go ahead and make the db call here
        if ($type == 'version') {
            $version = Version::find($id);
        }

        // Update Download limits tables
        if ($type == 'release') {
            $downloaded = DownloadedRelease::firstOrNew([
                'user' => $user,
                'song' => $id
            ]);
        } else if ($type == 'version') {
            $downloaded = DownloadedVersion::firstOrNew([
                'user' => $user,
                'version' => $id,
                'song' => $version->song
            ]);
        }

        if ($selector === 'dropbox') {
            $downloaded->drop_count++;
        } else if ($selector === 'crateq') {
            $downloaded->crateq_count++;
        } else {
            $downloaded->dl_count++;
        }

        $downloaded->save();

        unset($downloaded);

        if ($selector != 'crateq') {
            // Update Top Download metrics
            if ($type == 'release') {
                $downloaded = Release::find($id);
            } else if ($type == 'version') {
                $downloaded = Release::find($version->song);
            }

            $downloaded->downloads++;
            $downloaded->month_downloads++;

            $downloaded->save();

            unset($downloaded);

            // add download log
            $downloaded = new DownloadLog();

            $downloaded->user = $user;
            if ($type == 'release') {
                $downloaded->type = 'zip';
                $downloaded->song = $id;
            } else {
                $downloaded->type = 'song';
                $downloaded->song = $version->song;
            }
            $downloaded->id = $id;
            $downloaded->ip = $ip;
            $downloaded->downloaded = Carbon::now();
            $downloaded->info = '';

            $downloaded->save();
            unset($downloaded);
        }

        // Update Preferred Version information
        if ($type == 'release') {
            // Can't update preferred version since we're downloading a release

        } else if ($type == 'version') {

            $downloaded = PreferredVersion::firstOrNew([
                'user' => $user,
                'version' => $version->type
            ]);
    
            if ($selector === 'dropbox') {
                $downloaded->drop_count++;
            } else if ($selector === 'crateq') {
                $downloaded->crateq_count++;
            } else {
                $downloaded->dl_count++;
            }

            $downloaded->save();
    
            unset($downloaded);
        }
    }
}
