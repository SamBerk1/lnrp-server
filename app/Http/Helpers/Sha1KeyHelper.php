<?php
namespace App\Http\Helpers;

class Sha1KeyHelper
{
    public static function isExpired($key, $version)
    {
        if (strlen($key) < 40) {
            return true;
        }
        
        $key = strtolower($key);
        $today = sha1(date('Y-m-d').$version);
        $yesterday = sha1(date('Y-m-d', time() - 60 * 60 * 24).$version);
        $tomorrow = sha1(date('Y-m-d', time() + 60 * 60 * 24).$version);

        if ($key === $today || $key === $yesterday || $key === $tomorrow) {
            return false;
        }
        return true;
    }
}
