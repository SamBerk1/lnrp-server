<?php
namespace App\Http\Controllers;

use \App\Code;
use \App\User;
use \App\DownloadedRelease;
use \App\DownloadedVersion;
use \App\Release;
use \App\Version;
use \App\DownloadLog;

use Carbon\Carbon;

use \App\Http\Helpers\DownloadHelper;
use \App\Http\Helpers\Sha1KeyHelper;

use \Illuminate\Http\Request;

use \Illuminate\Database\Eloquent\ModelNotFoundException;

use \Tymon\JWTAuth\Facades\JWTAuth;

class DownloadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function requestDownload($release, $version = false)
    {
        $retoken = false;
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_expired'], 401);
            }
            JWTAuth::setToken($refreshed);
            $payload = JWTAuth::getPayload();
            $retoken = true;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_invalid'], 400);
        }

        if (!$payload->hasKey('username')) {
            return response()->json(['not_logged_in'], 401);
        }

        $user_id = $payload->get('id');
        $download_limit = app('db')->select("SELECT value FROM settings WHERE name = 'download_limit'");
        $download_limit = $download_limit[0]->value;
        $user_limit = app('db')->select("SELECT `limit` FROM download_limits WHERE user = $user_id");
        if ($user_limit) {
            $user_limit = $user_limit[0]->limit;
        }

        // Get Download limits tables Instance
        if ($version) {
            $downloaded = DownloadedVersion::firstOrNew([
                'user' => $user_id,
                'version' => $version,
                'song' => $release
            ]);
        } else {
            $downloaded = DownloadedRelease::firstOrNew([
                'user' => $user_id,
                'song' => $release
            ]);
        }
        $download_count = $downloaded->crateq_count + $downloaded->dl_count + $downloaded->drop_count;

        $isDownload = false;
        if ($download_count) {
            if ($user_limit) {
                if ($download_count >= $user_limit) {
                    $isDownload = true;
                }
            } else {
                if ($download_count >= $download_limit) {
                    $isDownload = true;
                }
            }
        }
        if ($isDownload) {
            return response("We're sorry, but you've already downloaded this song the maximum number of times. If you feel this is an error, please contact us.", 404);
        }
        unset($downloaded);
        // return response()->json([
        //     'dl_count' => $download_count,
        //     'limit' => $download_limit,
        //     'user_limit' =>$user_limit
        // ]);

        if (false === ($download_code = DownloadHelper::generateDownloadCode($user_id, $release, $version))) {
            return response('', 404);
        }

        if ($retoken) {
            $user = User::find($user_id);
            $token = JWTAuth::fromUser($user);
            return response()->json([
                'code' => $download_code->code,
                '_t' => $token
            ]);
        } else {
            return response()->json([
                'code' => $download_code->code
            ]);
        }
    }

    public function requestCrateQ($release, $version = false)
    {
        $retoken = false;
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_expired'], 401);
            }
            JWTAuth::setToken($refreshed);
            $payload = JWTAuth::getPayload();
            $retoken = true;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_invalid'], 400);
        }

        if (!$payload->hasKey('username')) {
            return response()->json(['not_logged_in'], 401);
        }

        $user_id = $payload->get('id');
        $download_limit = app('db')->select("SELECT `value` FROM settings WHERE name = 'download_limit'");
        $download_limit = $download_limit[0]->value;
        $user_limit = app('db')->select("SELECT `limit` FROM download_limits WHERE user = $user_id");
        if ($user_limit) {
            $user_limit = $user_limit[0]->limit;
        }

        // Get Download limits tables Instance
        if ($version) {
            $downloaded = DownloadedVersion::firstOrNew([
                'user' => $user_id,
                'version' => $version,
                'song' => $release
            ]);
        } else {
            $downloaded = DownloadedRelease::firstOrNew([
                'user' => $user_id,
                'song' => $release
            ]);
        }
        $download_count = $downloaded->crateq_count + $downloaded->dl_count + $downloaded->drop_count;

        $isDownload = false;
        if ($download_count) {
            if ($user_limit) {
                if ($download_count >= $user_limit) {
                    $isDownload = true;
                }
            } else {
                if ($download_count >= $download_limit) {
                    $isDownload = true;
                }
            }
        }
        if ($isDownload) {
            return response("We're sorry, but you've already downloaded this song the maximum number of times. If you feel this is an error, please contact us.", 404);
        }

        $downloaded->crateq_count++;
        $downloaded->save();
        unset($downloaded);

        if ($retoken) {
            $user = User::find($user_id);
            $token = JWTAuth::fromUser($user);
            return response()->json([
                'success' => true,
                'msg' => 'Your request has been successfully added to crateQ',
                '_t' => $token
            ], 200);
        } else {
            return response()->json([
                'success' => true,
                'msg' => 'Your request has been successfully added to crateQ'
            ], 200);
        }
    }

    public function requestDownloadValidation($version, Request $request) {
        $key = $request->input('key');
        $user_id = $request->input('userId');

        if ($key != env('CRATEQ_KEY')) {
            return response()->json([
                'errors' => true,
                'message' => 'Forbidden'
            ]);
        }

        if ($user_id === null) {
            return response()->json([
                'errors' => true,
                'message' => 'Invalid user id'
            ]);
        }
        $user = User::find($user_id);
        if ($user === null) {
            return response()->json([
                'errors' => true,
                'message' => 'User not found'
            ]);
        }

        if ($user->approved === 0 || $user->enabled === 0) {
            return response()->json([
                'errors' => true,
                'message' => 'You LNRP membership is not active. Please contact support.'
            ]);
        }

        $song_version = Version::find($version);
        if ($song_version === null) {
            return response()->json([
                'errors' => true,
                'message' => 'File not found.'
            ]);
        }

        // Get Download file
        $file = current(
            app('db')->select(
                'SELECT s.artist, s.title, sv.type AS `version`, sv.filename '.
                'FROM latenight.songs_versions sv '.
                'INNER JOIN latenight.songs s ON s.id = sv.song '.
                "WHERE sv.id = {$version}"
            )
        );

        $filename = "{$file->artist} - {$file->title} {$file->version}.mp3";
        $file = $file->filename;

        if (!is_file($file)) {
            return response()->json([
                'errors' => true,
                'message' => 'File not exist.'
            ]);
        }

        // Get Global & User Download limit from Settings
        $download_limit = app('db')->select("SELECT `value` FROM settings WHERE name = 'download_limit'");
        $download_limit = $download_limit[0]->value;
        $user_limit = app('db')->select("SELECT `limit` FROM download_limits WHERE user = $user_id");
        if ($user_limit) {
            $user_limit = $user_limit[0]->limit;
        }

        // Get Download limits tables Instance
        $downloaded = DownloadedVersion::firstOrNew([
            'user' => $user_id,
            'version' => $version,
            'song' => $song_version->song
        ]);
        $download_count = $downloaded->crateq_count + $downloaded->dl_count + $downloaded->drop_count;

        // Check Download limit
        $isDownload = false;
        if ($download_count) {
            if ($user_limit) {
                if ($download_count >= $user_limit) {
                    $isDownload = true;
                }
            } else {
                if ($download_count >= $download_limit) {
                    $isDownload = true;
                }
            }
        }
        if ($isDownload) {
            return response()->json([
                'errors' => true,
                'message' => 'You have exceed the number of times you are allowed to download this file. Contact support to have this file reset.'
            ]);
        }

        // Increase Download count
        $downloaded->dl_count++;
        $downloaded->save();
        unset($downloaded);

        return response()->json([
            'errors' => false,
            'message' => ''
        ]);
    }

    public function requestCrateQDownload($version, Request $request) {
        $token = $request->input('token');
        $user_id = $request->input('userId');

        if (Sha1KeyHelper::isExpired($token, $version)) {
            return response()->json([
                'errors' => true,
                'message' => 'invalid request'
            ], 400);
        }

        if ($user_id === null) {
            return response()->json([
                'errors' => true,
                'message' => 'invalid user id'
            ], 401);
        }
        $user = User::find($user_id);
        if ($user === null) {
            return response()->json([
                'errors' => true,
                'message' => 'User not found'
            ], 401);
        }

        $song_version = Version::find($version);
        if ($song_version === null) {
            return response()->json([
                'errors' => true,
                'message' => 'File not found.'
            ], 401);
        }

        // Get Download file
        $file = current(
            app('db')->select(
                'SELECT s.artist, s.title, sv.type AS `version`, sv.filename '.
                'FROM latenight.songs_versions sv '.
                'INNER JOIN latenight.songs s ON s.id = sv.song '.
                "WHERE sv.id = {$version}"
            )
        );

        $filename = "{$file->artist} - {$file->title} {$file->version}.mp3";
        $file = $file->filename;

        if (!is_file($file)) {
            return response()->json([
                'errors' => true,
                'message' => 'File not exist.'
            ], 401);
        }

        // add download log
        $downloaded = new DownloadLog();

        $downloaded->user = $user_id;
        $downloaded->type = 'song';
        $downloaded->song = $song_version->song;
        $downloaded->id = $version;
        $downloaded->ip = $request->ip();
        $downloaded->downloaded = Carbon::now();

        $downloaded->save();
        unset($downloaded);

        return response()->download(
            $file,
            $filename
        );
    }

    public function downloadFile(Request $request, $code, $type = false)
    {
        // Try to find the code in the database
        try {
            $download_code = \App\Code::findOrFail($code);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response('', 404);
        }

        // Check if code has been used
        if ($download_code->used == 1) {
            return response('Code has been used', 403);
        }

        $user = User::find($download_code->user);
        $clientIP = $request->ip();

        DownloadHelper::updateDownloadedCounts($download_code->user, $download_code->type, $download_code->id, $clientIP, $type);

        // Get filename from code
        list($file, $filename) = DownloadHelper::generateDownloadFiles($download_code);

        // Get filename to use in download
        // $filename = substr($file, strrpos($file, '/') + 1);

        // Confirm that file exists
        // TODO: Create logs for any time this happens
        if (!is_file($file)) {
            return response('File Not Found', 404);
        }

        $download_code->used = 1;
        $download_code->save();

        if ($download_code->type == 'release') {
            $header = ['Content-Disposition' => 'filename='.$filename, 'Content-Type' => 'application/zip'];
        } else {
            $header = ['Content-Disposition' => 'filename='.$filename, 'Content-Type' => 'audio/mpeg'];
        }

        return response()->download(
            $file,
            $filename,
            $header
        );
    }
}
