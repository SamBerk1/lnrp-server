<?php

namespace App\Http\Controllers;

// use GenTux\Jwt\JwtToken;
// use GenTux\Jwt\GetsJwtToken;
use IRequest;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\PayloadFactory;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;

class TokenController extends Controller
{
    // use GetsJwtToken;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create()
    {
        // $payload = ['exp' => time() + 7200]; // expire in 2 hours
        // $token = $jwt->createToken($payload); // new instance of JwtToken
        //
        // return response()->json([
        //     '_t' => $token
        // ]);

        // $payload = JWTFactory::isLoggedIn(true)->make();
        $payload = JWTFactory::make();

        $token = JWTAuth::encode($payload);

        return response()->json([
            '_t' => $token->get()
        ]);
    }
}
