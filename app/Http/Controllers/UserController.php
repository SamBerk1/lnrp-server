<?php
namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use \Illuminate\Support\Facades\Hash;

use \App\User as User;
use \App\ResetCode as ResetCode;
use \App\SetBilling as SetBilling;
use \App\Settings as Settings;

use \Tymon\JWTAuth\Facades\JWTAuth;
use \Cartalyst\Stripe\Laravel\Facades\Stripe;

use \Intervention\Image\ImageManager;

use \Illuminate\Support\Facades\Mail;
use App\Mail\ResetPassword;
use App\Mail\RenewAccount;
use App\Mail\RenewComplete;
use App\Mail\CompleteAccount;
use App\Mail\UserPromo;

class Cipher {
    private $securekey, $iv;

    function __construct($textkey) {
        $this->securekey = md5($textkey, TRUE);
        $this->iv = mcrypt_create_iv(32, MCRYPT_DEV_URANDOM);
    }

    function encrypt($input) {
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->securekey, $input, MCRYPT_MODE_ECB, $this->iv));
    }

    function decrypt($input) {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->securekey, base64_decode($input), MCRYPT_MODE_ECB, $this->iv));
    }
}

class UserController extends Controller
{
    public $cipher;

    public function __construct()
    {
        $this->cipher = new Cipher('Late Night Record Pool');
    }

    public function createAccount(Request $request)
    {
        $userInfo = $request->input();

        $password = $userInfo['password'];
        $userInfo['password'] = Hash::make($password);
        $userInfo['old_password'] = $this->cipher->encrypt($password);

        // Convert some booleans to other compatible values in the broke-ass database
        $userInfo = $this->prepareData($userInfo);

        $oldUser = User::where('username', $userInfo['username'])->first();
        if (!is_null($oldUser)) {
            return response()->json([
                'error' => 'Username already taken.'
            ], 400);
        }

        $oldUser = User::where('email', $userInfo['email'])->first();
        if (!is_null($oldUser)) {
            return response()->json([
                'error' => 'E-mail already taken.'
            ], 400);
        }

        $user = User::create($userInfo);

        // ...and convert them back, damnit
        $user = $this->revertData($user);

        // Clear out the password field in case of hinkyness
        $user->password = '';

        return response()->json($user);
    }

    public function createNewAccount(Request $request)
    {
        $inputData = $request->input();

        $userInfo = $inputData['user'];
        $promoCode = $inputData['promo'];

        $password = $userInfo['password'];
        $userInfo['password'] = Hash::make($password);
        $userInfo['old_password'] = $this->cipher->encrypt($password);

        // Convert some booleans to other compatible values in the broke-ass database
        $userInfo = $this->prepareData($userInfo);

        $oldUser = User::where('username', $userInfo['username'])->first();
        if (!is_null($oldUser)) {
            return response()->json([
                'error' => 'Username already taken.'
            ], 400);
        }

        $oldUser = User::where('email', $userInfo['email'])->first();
        if (!is_null($oldUser)) {
            return response()->json([
                'error' => 'E-mail already taken.'
            ], 400);
        }

        $user = User::create($userInfo);
        $user = $this->revertData($user);

        // Clear out the password field in case of hinkyness
        $user->password = '';

        $promo = Settings::where('name', 'promo_code')->first();
        if ($promo->enabled && $promo->value === $promoCode) {
            $billing = SetBilling::where('id', 1)->first();
            $return = [
                'user' => $user,
                'isPromo' => true,
                'billing' => $billing,
            ];

            // mail info
            $info = array(
                "code" => $promoCode,
                "name" => $user->name,
                "email" => $user->email
            );

            // Send the user a mail
            Mail::to(env('CONTACT_EMAIL'))
            ->cc(env('MANAGER_EMAIL'))
            ->bcc(env('DEVELOPER_EMAIL'))
            ->send(new UserPromo($info));
        } else {
            $return = [
                'user' => $user,
                'isPromo' => false,
            ];                
        }
        return response()->json($return);
    }

    public function updateAccount(Request $request, $id = null)
    {
        if ($id === null) {
            // User is updating via JWT
            try {
                JWTAuth::parseToken();
                $payload = JWTAuth::getPayload();
            } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
                try {
                    $refreshed = JWTAuth::refresh();
                } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                    return response()->json(['token_expired'], 401);
                }
                JWTAuth::setToken($refreshed);
                $payload = JWTAuth::getPayload();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_invalid'], 400);
            }

            if (!$payload->hasKey('username')) {
                return response()->json(['not_logged_in'], 401);
            }

            $payload = $request->input();

            $user = User::find($payload['id']);

            $userInfo = $request->input();

            // Check for updated password and hash if provided
            if (isset($userInfo['password']) && $userInfo['password'] !== '') {
                $password = $userInfo['password'];
                $userInfo['password'] = Hash::make($password);
                $userInfo['old_password'] = $this->cipher->encrypt($password);
            } else {
                $userInfo['password'] = $user['password'];
                $userInfo['old_password'] = $user['old_password'];
            }

            // Clear out user image file, as well
            unset($userInfo['image']);

        } else {
            // User is creating their account and is not logged in
            $user = User::find($id);

            $userInfo = $request->input();
            unset($userInfo['id']);

            if (isset($userInfo['password']) && $userInfo['password'] !== '') {
                $password = $userInfo['password'];
                $userInfo['password'] = Hash::make($password);
                $userInfo['old_password'] = $this->cipher->encrypt($password);
            } else {
                $userInfo['password'] = $user['password'];
                $userInfo['old_password'] = $user['old_password'];
            }
        }

        if ($user === null) {
            return response('User not found', 404);
        }

        $userInfo = $this->prepareData($userInfo);

        $user->fill($userInfo);

        $user->save();

        if ($id === null) {
            // Updating via account page, send back JWT
            return response()->json([
                'success' => true,
                '_t' => JWTAuth::fromUser($user)
            ]);
        } else {
            // Still in signup flow, send back basic user object
            return response()->json($user);
        }
    }

    public function getImage($id = false, $width = false, $height = false)
    {
        // Check if ID value for user was provided
        if ($id === false)
        {
            return response('Invalid ID', 400);
        }

        // Initialize ImageManager
        $imageManager = new ImageManager(['driver' => 'gd']);

        // Get User information
        $user = User::find($id);

        if ($user->image !== '') {
            $user_image = storage_path() . "/app/user_images/{$user->image}";

        } else {
            $user_image = storage_path() . "/app/genre_images/default.jpg";
        }

        // Check if user image exists and get dimension information
        if (!is_null($user_image) && is_file($user_image)) {
            list($image_info['width'], $image_info['height']) = getimagesize($user_image);

        } else {
            // No image file found for the user. Setting defaults
            $image_info = [
                'width' => 300,
                'height' => 300
            ];
        }

        // Determine new widths and heights
        if ($width === false) {
            // No width was provided, return original image
            $new_image['width'] = $image_info['width'];
            $new_image['height'] = $image_info['height'];

        } else {
            // Width was provided. Check for height
            if ($height === false) {
                // No height was provided. Calculate relative height based on provided width

                // Calculate width/height ratio
                $ratio = $image_info['height'] / $image_info['width'];

                // Apply ration to width to get new height
                $height = $width * $ratio;

            } else {
                // Height was provided
            }
        }

        //$image = $imageManager->canvas($width, $height);

        return $imageManager->make($user_image)->resize($width, $height)->response();
    }

    public function completeAccount(Request $request, $id = null)
    {
        if ($id === null) {
            return response('really?', 500);
        }

        $user = User::find($id);

        if ($user === null) {
            return response('No user found', 404);
        }

        $info = $request->input();

        // Create the Stripe user
        $customer = Stripe::customers()->create([
            'description' => "{$user['id']} - {$user['name']}",
            'source' => array(
                'object' => 'card',
                'number' => $info['cardNumber'],
                'exp_month' => $info['cardExpMonth'],
                'exp_year' => $info['cardExpYear'],
                'cvc' => $info['cardCVV'],
                'address_zip' => $info['cardZip']
            ),
            'email' => $user['email']
        ]);

        // Subscribe the user to a payment plan
        $subscription = Stripe::subscriptions()->create(
            $customer['id'],
            [
                'plan' => $info['subscription']
            ]
        );

        // Set all the payment information to the user record
        $user->stripe_id = $customer['id'];
        $user->stripe_subscription = $subscription['id'];
        $user->subscription = $info['subscription'];
        $user->card_type = current($customer['sources']['data'])['brand'];
        $user->card_last_four = current($customer['sources']['data'])['last4'];
        $user->card_exp = current($customer['sources']['data'])['exp_month'] . '/' . current($customer['sources']['data'])['exp_year'];

        // Enable/Activate the user in the database
        $user->enabled = 1;
        $user->approved = 1;
        $user->type = 'Regular';

        // Save the changes
        $user->save();

        // Send the user a mail
        Mail::to($user->email)
            ->send(new CompleteAccount($info));

        return response()->json([
            'success' => true,
            '_t' => JWTAuth::fromUser($user),
            'user' => $user
        ]);
    }

    public function updateCard(Request $request, $id = null)
    {
        if ($id === null) {
            return response('really?', 500);
        }

        $user = User::find($id);

        if ($user === null) {
            return response('No user found', 404);
        }

        $info = $request->input();

        $card = current(Stripe::cards()->all($user->stripe_id)['data']);

        if ($card) {
            Stripe::cards()->delete($user->stripe_id, $card['id']);
        }

        $res = Stripe::cards()->create($user->stripe_id, [
            'number' => $info['cardNumber'],
            'exp_month' => $info['cardExpMonth'],
            'exp_year' => $info['cardExpYear'],
            'cvc' => $info['cardCVV'],
            'address_zip' => $info['cardZip']
        ]);

        $user->card_type = $res['brand'];
        $user->card_last_four = $res['last4'];
        $user->payment_type = "Stripe";
        $user->card_exp = $res['exp_month'] . "/" . $res['exp_year'];

        $user->save();

        return response()->json([
            'success' => true,
            '_t' => JWTAuth::fromUser($user),
            'user' => $user
        ]);
    }

    public function sendResetLink(Request $request)
    {
        $input = $request->input();

        // Check if an email was actually provided
        if (!isset($input['email']) || $input['email'] === '') {
            return response()->json(['error' => 'No email provided'], 500);
        }

        // Find user record based on this email
        $user = User::where(['email' => $input['email']])->first();

        if ($user === null) {
            return response()->json(['error' => 'A user was not found with that email address. Please check your information and try again.'], 404);
        }

        $code = new ResetCode;

        $code->code = str_random(40);
        $code->user = $user->id;
        $code->created = date('Y-m-d H:i:s');

        $code->save();

        $template = [];
        $template['reset_link'] = env('WEB_URL') . "/password-reset/{$code->code}";

        Mail::to($user->email)
            ->send(new ResetPassword($template));

        return response()->json([
            'success' => true,
            'user' => $user,
            'temp' => $template
        ]);
    }

    public function resetPassword(Request $request)
    {
        $input = $request->input();

        // Check if reset code is available
        if (!isset($input['code']) || $input['code'] == '') {
            return response()->json(['error' => 'No verification code provided'], 500);
        }

        // Try and find a valid code
        $code = ResetCode::find($input['code']);

        if ($code === null) {
            return response()->json(['error' => 'The code provided was not valid. Please check your information and try again.'], 500);
        }

        if ($code->used === 1) {
            return response()->json(['error' => 'The code provided provided has already been used. Please check your information and try again.'], 500);
        }

        // Code is valid, find and update the user record
        $user = User::find($code->user);

        if ($user === null) {
            return response()->json(['error' => 'The user attached to this code could not be found. Please check your information and try again.'], 500);
        }

        $user->password = Hash::make($input['password']);
        $user->old_password = $this->cipher->encrypt($input['password']);
        $user->save();

        // Mark the code as used and send back a JWT
        $code->used = 1;
        $code->save();

        return response()->json([
            'success' => true,
            '_t' => JWTAuth::fromUser($user)
        ]);
    }

    public function sendRenewLink(Request $request)
    {
        $input = $request->input();

        // Check if an email was actually provided
        if (!isset($input['email']) || $input['email'] === '') {
            return response()->json(['error' => 'No email provided'], 500);
        }

        // Find user record based on provided email and username values
        $user = User::where([
            'username' => $input['username'],
            'email' => $input['email']
        ])->first();

        if ($user === null) {
            return response()->json(['error' => 'A user was not found with that email address and username. Please check your information and try again.'], 404);
        }

        $code = new ResetCode;

        $code->code = str_random(40);
        $code->user = $user->id;
        $code->created = date('Y-m-d H:i:s');

        $code->save();

        $template = [];
        $template['renew_link'] = env('WEB_URL') . "/renew-account/{$code->code}";

        Mail::to($user->email)
            ->send(new RenewAccount($template));

        return response()->json([
            'success' => true,
            'code' => $code->code
        ], 200);
    }


    /*** user ip address ***/
    public function updateIPaddress(Request $request) {

        $input = $request->input();
        $ip_address = $input['ip_address'];
        $retoken = false;
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_expired'], 401);
            }
            JWTAuth::setToken($refreshed);
            $payload = JWTAuth::getPayload();
            $retoken = true;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_invalid'], 400);
        }

        if (!$payload->hasKey('username')) {
            return response()->json(['not_logged_in'], 401);
        }

        $user_id = $payload->get('id');

        if (is_null($user_id)) {
            return response()->json(['token_invalid'], 400);
        }

        $user = User::find($user_id);
        $user->ip_address = $ip_address;

        $user->save();

        if ($retoken) {
            return response()->json([
                'success' => true,
                'user' => $user,
                '_t' => JWTAuth::fromUser($user)
            ]);
        } else {
            return response()->json([
                'success' => true,
                'user' => $user
            ]);
        }
    }

    /****** unlink dropbox account ******/
    public function unlinkDropboxAccount () {
        $retoken = false;
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_expired'], 401);
            }
            JWTAuth::setToken($refreshed);
            $payload = JWTAuth::getPayload();
            $retoken = true;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_invalid'], 400);
        }

        if (!$payload->hasKey('username')) {
            return response()->json(['not_logged_in'], 401);
        }

        $user_id = $payload->get('id');
        $dropbox_token = User::find($user_id)->dropbox;

        $result = false;
        if ($dropbox_token) {
            $result = $dropbox_token->delete();
        } else {
            return response()->json(['error' => 'linked dropbox account not found'], 404);
        }

        if ($result) {
            if ($retoken) {
                $user = User::find($user_id);
                $token = JWTAuth::fromUser($user);
                return response()->json([
                    'success' => true,
                    'msg' => 'unlinked your dropbox account successfully',
                    't' => $token
                ], 200);
            } else {
                return response()->json([
                    'success' => true,
                    'msg' => 'unlinked your dropbox account successfully'
                ], 200);
            }
        } else {
            return response()->json(['error' => 'Bad request'], 400);
        }
    }

    public function promoValidation(Request $request)
    {
        $input = $request->input();
        $renew = $input['code'];
        $code = $input['promo'];

        if ($code === null) {
            // No code was found. Hackity hack hack
            return response('No promo code was sent with this request', 400);
        }

        if ($renew === null) {
            // No code was found. Hackity hack hack
            return response('An invalid or no renewal code was sent with this request', 400);
        }

        $renew = ResetCode::find($renew);
        if ($renew === null) {
            return response('The renew code provided was not valid. Please check your information and try again.', 400);
        }

        // Code is valid, find and update the user record
        $user = User::find($renew->user);
        if ($user === null) {
            return response('The user attached to this code could not be found. Please check your information and try again.', 400);
        }

        $promo = Settings::where('name', 'promo_code')->first();
        if ($promo->enabled && $promo->value === $code) {
            $billing = SetBilling::where('id', 1)->first();
            $return = [
                'isValid' => true,
                'billing' => $billing,
            ];

            // mail info
            $info = array(
                "code" => $code,
                "name" => $user->name,
                "email" => $user->email
            );

            // Send the user a mail
            Mail::to(env('CONTACT_EMAIL'))
                ->cc(env('MANAGER_EMAIL'))
                ->bcc(env('DEVELOPER_EMAIL'))
                ->send(new UserPromo($info));
        } else {
            $return = [
                'isValid' => false,
            ];
        }

        return response()->json($return);
    }

    public function renewUser(Request $request, $code = null)
    {
        if ($code === null) {
            // No code was found. Hackity hack hack
            return response('An invalid or no renewal code was sent with this request', 500);
        }

        $code = ResetCode::find($code);

        if ($code === null) {
            return response('The code provided was not valid. Please check your information and try again.', 500);
        }

        if ($code->used === 1) {
            return response('The code provided provided has already been used. Please check your information and try again.', 500);
        }

        // Code is valid, find and update the user record
        $user = User::find($code->user);

        if ($user === null) {
            return response('The user attached to this code could not be found. Please check your information and try again.', 500);
        }

        $info = $request->input();

        // Create the Stripe user
        $customer = Stripe::customers()->create([
            'description' => "{$user['id']} - {$user['name']}",
            'source' => array(
                'object' => 'card',
                'number' => $info['cardNumber'],
                'exp_month' => $info['cardExpMonth'],
                'exp_year' => $info['cardExpYear'],
                'cvc' => $info['cardCVV'],
                'address_zip' => $info['cardZip']
            ),
            'email' => $user['email']
        ]);

        // Subscribe the user to a payment plan
        try {
            $subscription = Stripe::subscriptions()->create(
                $customer['id'],
                [
                    'plan' => $info['subscription']
                ]
            );
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            return response()->json([
                'success' => false,
                'error' => $e->getMessage()
            ], 400);
        }

        // Set all the payment information to the user record
        $user->stripe_id = $customer['id'];
        $user->stripe_subscription = $subscription['id'];
        $user->subscription = $info['subscription'];
        $user->card_type = current($customer['sources']['data'])['brand'];
        $user->card_last_four = current($customer['sources']['data'])['last4'];
        $user->card_exp = current($customer['sources']['data'])['exp_month'] . '/' . current($customer['sources']['data'])['exp_year'];

        // Enable/Activate the user in the database
        $user->enabled = 1;
        $user->approved = 1;
        $user->type = 'Regular';

        // Save the changes
        $user->save();

        // Mark the code as used and send back a JWT
        $code->used = 1;
        $code->save();

        // Send the user a mail
        Mail::to($user->email)
            ->send(new RenewComplete($info));

        return response()->json([
            'success' => true,
            '_t' => JWTAuth::fromUser($user),
            'user' => $user
        ]);
    }

    private function prepareData($info)
    {
        if (array_key_exists("equipment_mac", $info)) {
            $info['equipment_mac'] = (int) $info['equipment_mac'];
        } else {
            $info['equipment_mac'] = 0;
        }
        
        if (array_key_exists("equipment_pc", $info)) {
            $info['equipment_pc'] = (int) $info['equipment_pc'];
        } else {
            $info['equipment_pc'] = 0;
        }
        
        if (array_key_exists("equipment_turntables", $info)) {
            $info['equipment_turntables'] = (int) $info['equipment_turntables'];
        } else {
            $info['equipment_turntables'] = 0;
        }
        
        if (array_key_exists("radiostation", $info)) {
            $info['radiostation'] = $info['radiostation'] ? 'yes' : 'no';
        } else {
            $info['radiostation'] = 'no';
        }

        if (array_key_exists("club", $info)) {
            $info['club'] = $info['club'] ? 'yes' : 'no';
        } else {
            $info['club'] = 'no';
        }

        if (array_key_exists("house", $info)) {
            $info['house'] = $info['house'] ? 'yes' : 'no';
        } else {
            $info['house'] = 'no';
        }

        if (array_key_exists("festival", $info)) {
            $info['festival'] = $info['festival'] ? 'yes' : 'no';
        } else {
            $info['festival'] = 'no';
        }

        if (array_key_exists("dropbox_info", $info)) {
            unset($info['dropbox_info']);
        }

        if (array_key_exists("dropbox", $info)) {
            unset($info['dropbox']);
        }

        if (array_key_exists("setRefreshTTL", $info)) {
            unset($info['setRefreshTTL']);
        }

        if (array_key_exists("prv", $info)) {
            unset($info['prv']);
        }

        if (array_key_exists("iss", $info)) {
            unset($info['iss']);
        }

        if (array_key_exists("iat", $info)) {
            unset($info['iat']);
        }

        if (array_key_exists("exp", $info)) {
            unset($info['exp']);
        }

        if (array_key_exists("nbf", $info)) {
            unset($info['nbf']);
        }

        if (array_key_exists("jti", $info)) {
            unset($info['jti']);
        }

        if (array_key_exists("sub", $info)) {
            unset($info['sub']);
        }

        if (array_key_exists("versions", $info)) {
            unset($info['versions']);
        }

        if (array_key_exists("releases", $info)) {
            unset($info['releases']);
        }

        return $info;
    }

    private function revertData($info)
    {
        $info->equipment_mac = (bool) $info->equipment_mac;
        $info->equipment_pc = (bool) $info->equipment_pc;
        $info->equipment_turntables = (bool) $info->equipment_turntables;

        $info->radiostation = $info->radiostation == 'yes';
        $info->club = $info->club == 'yes';
        $info->house = $info->house == 'yes';
        $info->festival = $info->festival == 'yes';

        return $info;
    }
}
