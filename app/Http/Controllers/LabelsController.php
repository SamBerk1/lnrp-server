<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LabelsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAllLabels()
    {
        $labels = app('db')
                    ->select(
                        'SELECT label AS `name`, COUNT(*) AS `total` '.
                        'FROM latenight.songs '.
                        'GROUP BY label '.
                        'HAVING `total` > 20 AND label != "" '.
                        'ORDER BY label ASC'
                    );

        return response()->json($labels);
    }
}
