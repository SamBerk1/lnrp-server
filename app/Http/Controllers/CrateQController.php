<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\User as User;
use \App\ResetCode as ResetCode;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Factory;
use Tymon\JWTAuth\Facades\JWTFactory;

class CrateQController extends Controller
{
    //get content
    public function getContentData($start = 0, $limit = 10) {

        if (!(is_numeric($start) && is_numeric($limit))) {
            return response('Invalid request', 400);
        }

        $query = "SELECT * FROM (SELECT s.id, `artist`, `title`, `genre_id` AS `genreId`, `genre`, ".
            "sv.`time` AS `length`, sv.bpm, sv.type AS `mix`, sv.song AS `groupId`, ".
            "IF ((sv.type = 'clean' || sv.type = 'dirty'), sv.type, null) AS `advisory`, ".
            "IF (s.title LIKE '%(%', s.title, null) AS `editor`, s.year AS `release_year`, ".
            "s.label AS `record_label`, FROM_UNIXTIME(s.date, '%Y-%m-%dT%TZ') AS `playlist_date`, ".
            "s.downloads as `rank` FROM songs s LEFT JOIN songs_versions sv ON s.id = sv.song) ".
            "AS temp GROUP BY `id` LIMIT {$start}, {$limit}";

        $contents = app('db')->select($query);

        if ($contents) {
            foreach($contents as $item) {
                if ($item->editor) {
                    $start_pos = stripos($item->editor, '(') + 1;
                    $end_pos = strripos($item->editor, ')');
                    $str = substr($item->editor, $start_pos, $end_pos - $start_pos);
                    $item->editor = $str;
                }
                $item->preview = env('FILE_URL').$item->id;
            }
        }

        $return = [
            'contents' => $contents
        ];

        return response()->json($return);
    }

    public function login(Request $request)
    {
        $input = $request->only('username', 'password');

        $username = $input['username'];
        $password = $input['password'];

        $user = User::where('username', $username)->first();

        $msg = null;
        $userId = 0;

        if (is_null($user)) {
            $userId = 0;
            $msg = 'Invalid username and/or password.';
        } else if (!Hash::check($input['password'], $user->password)) {
            $userId = 0;
            $msg = 'Invalid username and/or password.';
        } else if ($user->approved === 0) {
            $userId = 0;
            $msg = 'Invalid username and/or password.';
        } else if ($user->enabled === 0) {
            $userId = $user->id;
            $msg = 'You LNRP account is disabled. Please visit latenightrecordpool.com to check your subscription status or contact support.';
        } else if ($user->type === 'In Progress') {
            $userId = $user->id;
            $msg = 'Sorry,  your LNRP account is in progress. Please try again later.';
        } else if ($user->type === 'Incomplete' || $user->type === 'VIP' || $user->type === 'Regular') {
            $userId = $user->id;
            $msg = null;
        }

        return response()->json([
            "userId" => $userId,
            "msg" => $msg,
            "email" => is_null($user) ? '' : $user->email,
            "status" => is_null($user) ? '' : 'active',
            "plan" => is_null($user) ? '' : $user->crateq_plan,
            "country" => "eu",
            "crateq" => true
        ]);
    }

    public function getGenres(Request $request) {

        $input = $request->only('origin', 'key');
        $origin = $input['origin'];
        $key = $input['key'];

        if ($origin != 'crateq' || $key != env('CRATEQ_KEY')) {
            return response()->json([
                'errors' => true,
                'msg' => 'invalid request'
            ], 400);
        }

        $genres = app('db')
                    ->select(
                        'SELECT CONVERT(genres.id, CHAR(5)) AS categoryId, genres.name AS category, genres_sub.id AS genreId, genres_sub.name AS title '.
                        'FROM latenight.songs_genres_sub genres_sub '.
                        'INNER JOIN latenight.songs_genres genres ON genres_sub.parent = genres.id '.
                        'ORDER BY category ASC'
                    );

        return response()->json($genres);
    }

    public function getReleases(Request $request) {

        $key = $request->input('key');
        if ($key != env('CRATEQ_KEY')) {
            return response()->json([
                'status' => 403,
                'message' => 'Forbidden'
            ], 403);
        }

        $limit = 100;
        $input = $request->only('sort', 'dir', 'page', 'userId', 'genreCategoryId', 'chart');

        $direct = isset($input['dir']) ? $input['dir'] : 'desc';
        $sort = isset($input['sort']) ? $input['sort'] : 'date';
        $order = "ORDER BY res." . $sort . " " . $direct;
        $page = isset($input['page']) ? $input['page'] : 1;
        $start = ($page - 1) * $limit;
        $userId = isset($input['userId']) ? $input['userId'] : '';
        $where = isset($input['genreCategoryId']) ? "WHERE s.genre_id = " . $input['genreCategoryId'] : '';

        $chart = isset($input['chart']) ? $input['chart'] : null;

        if ($chart == null) {
            $query = "SELECT * FROM (SELECT sv.id AS `mediaId`, s.artist, ".
                    "CONCAT(s.title, ' [', (CASE WHEN INSTR(LCASE(sv.type), 'acc') > 0 THEN 'Acapella' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'acc in') > 0 THEN 'BlenX-in' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'acc out') > 0 THEN 'BlenX' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'extended') > 0 THEN 'Xtendz' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'intro') > 0 THEN 'Intro' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'short') > 0 THEN 'Snipz' ELSE 'Single' END END END END END END), '] - ', ".
                    "IF (INSTR(LCASE(sv.type), 'dirty') > 0, 'Dirty', 'Clean')) AS `title`, s.id AS `groupId`, ".
                    "IF (s.subgenre_id > 0, s.subgenre_id, s.genre_id) AS `genreId`, ".
                    "IF (s.subgenre_id > 0, sg.name, g.name) AS `genre`, ".
                    "s.genre_id AS `genreCategoryId`, ifnull(g.name, s.genre) AS `genreCategory`, sv.bpm, sv.time AS `length`, '' AS editor, ".
                    "IF (INSTR(LCASE(sv.type), 'dirty') > 0, 'Dirty', 'Clean') AS `advisory`, ".
                    "IF(s.year > 0, s.year, YEAR(DATE(FROM_UNIXTIME(s.date)))) AS `releaseYear`, ".
                    "CONCAT('". env('PREVIEW_URL') ."', sv.id) AS `preview`, ".
                    "DATE(ifnull(s.updated, FROM_UNIXTIME(s.date))) AS `date`, true AS `audio`, false AS `video` ".
                    "FROM songs_versions sv ".
                    "INNER JOIN songs s ON s.id = sv.song ".
                    "LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id ".
                    "LEFT JOIN songs_genres g ON s.genre_id = g.id " . $where . ") AS res " . $order . " LIMIT " . $start . ", " . $limit;

            $media = app('db')->select($query);

            $count_query = "SELECT COUNT(*) as total FROM songs_versions sv ".
                            "INNER JOIN songs s ON s.id = sv.song ".
                            "LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id ".
                            "LEFT JOIN songs_genres g ON s.genre_id = g.id ". $where;
            $records =  current(app('db')->select($count_query));
            $pages = ceil($records->total / $limit);

            $return = [
                'records' => $records->total,
                'message' => $start . "-" . ($start+$limit),
                'pages' => $pages,
                'page' => $page,
                'sort' => $sort,
                'dir' => $direct,
                'rows' => $limit,
                'data' => $media
            ];

            return response()->json($return);
        } else if ($chart == 'trending') {
            $top_query = "SELECT sv.id, COUNT(*) AS cnt FROM songs_versions sv INNER JOIN download_log d ON sv.song = d.song WHERE (d.type = 'song' OR d.type = 'zip') AND d.song > 0 AND d.downloaded >= DATE(NOW()) + INTERVAL -7 DAY GROUP BY sv.id ORDER BY cnt DESC LIMIT 100";
            $songs = app('db')->select($top_query);

            $records = count($songs);

            $ids = array();
            foreach ($songs as $song) {
                array_push($ids, $song->id);
            }

            $query = "SELECT * FROM (SELECT sv.id AS `mediaId`, s.artist, ".
                    "CONCAT(s.title, ' [', (CASE WHEN INSTR(LCASE(sv.type), 'acc') > 0 THEN 'Acapella' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'acc in') > 0 THEN 'BlenX-in' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'acc out') > 0 THEN 'BlenX' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'extended') > 0 THEN 'Xtendz' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'intro') > 0 THEN 'Intro' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'short') > 0 THEN 'Snipz' ELSE 'Single' END END END END END END), '] - ', ".
                    "IF (INSTR(LCASE(sv.type), 'dirty') > 0, 'Dirty', 'Clean')) AS `title`, s.id AS `groupId`, ".
                    "IF (s.subgenre_id > 0, s.subgenre_id, s.genre_id) AS `genreId`, ".
                    "IF (s.subgenre_id > 0, sg.name, g.name) AS `genre`, ".
                    "s.genre_id AS `genreCategoryId`, ifnull(g.name, s.genre) AS `genreCategory`, sv.bpm, sv.time AS `length`, '' AS editor, ".
                    "IF (INSTR(LCASE(sv.type), 'dirty') > 0, 'Dirty', 'Clean') AS `advisory`, ".
                    "IF(s.year > 0, s.year, YEAR(DATE(FROM_UNIXTIME(s.date)))) AS `releaseYear`, ".
                    "CONCAT('". env('PREVIEW_URL') ."', sv.id) AS `preview`, ".
                    "DATE(ifnull(s.updated, FROM_UNIXTIME(s.date))) AS `date`, true AS `audio`, false AS `video` ".
                    "FROM songs_versions sv ".
                    "INNER JOIN songs s ON s.id = sv.song ".
                    "LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id ".
                    "LEFT JOIN songs_genres g ON s.genre_id = g.id WHERE sv.id IN (" . implode(',', $ids) . ")) AS res " . $order;
            
            $media = app('db')->select($query);
            $pages = ceil($records / $limit);

            $return = [
                'records' => $records,
                'message' => $start . "-" . ($start+$limit),
                'pages' => $pages,
                'page' => $page,
                'sort' => $sort,
                'dir' => $direct,
                'rows' => $limit,
                'data' => $media
            ];
            return response()->json($return);
        } else if ($chart == 'monthly') {
            $query = "SELECT * FROM (SELECT sv.id AS `mediaId`, s.artist, ".
                    "CONCAT(s.title, ' [', (CASE WHEN INSTR(LCASE(sv.type), 'acc') > 0 THEN 'Acapella' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'acc in') > 0 THEN 'BlenX-in' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'acc out') > 0 THEN 'BlenX' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'extended') > 0 THEN 'Xtendz' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'intro') > 0 THEN 'Intro' ELSE ".
                    "CASE WHEN INSTR(LCASE(sv.type), 'short') > 0 THEN 'Snipz' ELSE 'Single' END END END END END END), '] - ', ".
                    "IF (INSTR(LCASE(sv.type), 'dirty') > 0, 'Dirty', 'Clean')) AS `title`, s.id AS `groupId`, ".
                    "IF (s.subgenre_id > 0, s.subgenre_id, s.genre_id) AS `genreId`, ".
                    "IF (s.subgenre_id > 0, sg.name, g.name) AS `genre`, ".
                    "s.genre_id AS `genreCategoryId`, ifnull(g.name, s.genre) AS `genreCategory`, sv.bpm, sv.time AS `length`, '' AS editor, ".
                    "IF (INSTR(LCASE(sv.type), 'dirty') > 0, 'Dirty', 'Clean') AS `advisory`, ".
                    "IF(s.year > 0, s.year, YEAR(DATE(FROM_UNIXTIME(s.date)))) AS `releaseYear`, ".
                    "CONCAT('". env('PREVIEW_URL') ."', sv.id) AS `preview`, ".
                    "DATE(ifnull(s.updated, FROM_UNIXTIME(s.date))) AS `date`, true AS `audio`, false AS `video` ".
                    "FROM songs_versions sv ".
                    "INNER JOIN songs s ON s.id = sv.song ".
                    "LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id ".
                    "LEFT JOIN songs_genres g ON s.genre_id = g.id ORDER BY s.month_downloads DESC LIMIT 100) AS res " . $order;

            $media = app('db')->select($query);

            $return = [
                'records' => $limit,
                'message' => $start . "-" . ($start+$limit),
                'pages' => 1,
                'page' => $page,
                'sort' => $sort,
                'dir' => $direct,
                'rows' => $limit,
                'data' => $media
            ];
            return response()->json($return);
        }

        return response()->json([
            'status' => 400,
            'message' => 'Invalid Request'
        ], 400);
    }

    public function getCrateQ(Request $request) {
        $key = $request->input('key');
        if ($key != env('CRATEQ_KEY')) {
            return response()->json([
                'status' => 403,
                'message' => 'Forbidden'
            ], 403);
        }

        $input = $request->only('date');
        $date = isset($input['date']) ? $input['date'] : "2018-01-01";

        $media = app('db')->select('CALL latenight.crateq_songs(?)', array($date));
        return response()->json($media);
    }

    public function getOneSong($id, Request $request) {
        $key = $request->input('key');
        if ($key != env('CRATEQ_KEY')) {
            return response()->json([
                'status' => 403,
                'message' => 'Forbidden'
            ], 403);
        }
        $media = app('db')->select('CALL latenight.crateq_song(?)', array($id));
        if (count($media)>0) {
            return response()->json(current($media));
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Mp3 File Not Found'
            ], 404);
        }
    }

    public function playPreview($id)
    {
        $info = app('db')->select(
            "SELECT `type`, `time`, `filename` FROM songs_versions WHERE `id` = {$id} LIMIT 1"
        );

        $info = current($info);

        $filename = $info->filename;

        if (!is_file($filename)) {
            return response()->json([
                'status' => 404,
                'message' => 'Mp3 File Not Found'
            ], 404);
        }

        return response()->file($filename, [
            'Content-Type' => 'audio/mp3',
            'Content-Length' => filesize($filename)
        ]);
    }

    public function recentDownload(Request $request)
    {
        $key = $request->input('key');
        if ($key != env('CRATEQ_KEY')) {
            return response()->json([
                'status' => 403,
                'message' => 'Forbidden'
            ], 403);
        }

        $recentVersions = app('db')->select('CALL latenight.crateq_downloads_list()');

        $downloadCountList = array();
        foreach ($recentVersions as $version) {
            $downloads = current(app('db')->select('CALL latenight.crateq_downloads(?)', array($version->originatorId)));
            array_push($downloadCountList, $downloads);
        }
        return response()->json($downloadCountList);
    }

    public function getCrateQDownload($id, Request $request)
    {
        $key = $request->input('key');
        if ($key != env('CRATEQ_KEY')) {
            return response()->json([
                'status' => 403,
                'message' => 'Forbidden'
            ], 403);
        }

        $downloads = current(app('db')->select('CALL latenight.crateq_downloads(?)', array($id)));
        return response()->json($downloads);
    }

    public function getCrateQDownloadList(Request $request)
    {
        $key = $request->input('key');
        if ($key != env('CRATEQ_KEY')) {
            return response()->json([
                'status' => 403,
                'message' => 'Forbidden'
            ], 403);
        }

        $downloadVersions = app('db')->select('CALL latenight.crateq_downloads_list()');
        
        $downloadList = array();
        foreach ($downloadVersions as $version) {
            array_push($downloadList, $version->originatorId);
        }

        return response()->json($downloadList);
    }

    public function downloadCrateQ($type = false) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, env('CRATEQ_VERSION'));
        $contents = curl_exec($ch);
        curl_close($ch);
        $xmlValues = simplexml_load_string($contents);
        if (!$xmlValues) {
            return response()->json([
                'success' => false,
                'message' => 'Not found version info from installs.crateq.com'
            ], 404); 
        }
        $version = current($xmlValues->version);

        if ($type) {
            if ($type === 'win' || $type === 'windows') {
                $file = env('CRATEQ_INSTALL') . '/latest-version/crateQ_win_v' . $version . '.exe';
            } else {
                $file = env('CRATEQ_INSTALL') . '/latest-version/crateQ_mac_v' . $version . '.dmg';
            }
            return response()->json([
                'file' => $file,
                'type' => $type,
                'version' => $version
            ]);
        }

        $mac = env('CRATEQ_INSTALL') . '/latest-version/crateQ_mac_v' . $version . '.dmg';
        $win = env('CRATEQ_INSTALL') . '/latest-version/crateQ_win_v' . $version . '.exe';

        return response()->json([
            'version' => $version,
            'mac' => $mac,
            'win' => $win
        ]);
    }
}
