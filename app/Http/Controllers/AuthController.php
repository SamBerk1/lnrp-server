<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

use App\User as User;
use \App\ResetCode as ResetCode;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Factory;
use Tymon\JWTAuth\Facades\JWTFactory;

class AuthController extends Controller
{
    public function __construct()
    {
        //
    }

    public function doLogin(Request $request)
    {
        $input = $request->only('username', 'password', 'ip_address');

        $username = $input['username'];
        $password = $input['password'];
        $ip_address = $input['ip_address'];

        $user = User::where('username', $username)->first();

        if (is_null($user)) {
            return response()->json([
                'error' => 'A user account for the username provided cannot be found. Please check your information and try again.',
                'value' => 'invalid user'
            ], 404);
        }

        if (!Hash::check($input['password'], $user->password)) {
            return response()->json([
                'error' => 'The password provided does not match our records. Please check your information and try again.',
                'value' => 'invalid password'
            ], 400);
        }

        $user->ip_address = $ip_address;
        $user->save();

        if ($user->enabled === 0) {
            if ($user->approved === 0) {
                return response()->json([
                    'error' => 'Your account has been deactivated. Please contact customer support.',
                    'value' => 'review'
                ], 405);
            }

            $code = new ResetCode;
            $code->code = str_random(40);
            $code->user = $user->id;
            $code->created = date('Y-m-d H:i:s');

            $code->save();

            return response()->json([
                'error' => 'Your account has not yet been enabled. If you feel this is in error, please contact customer support.',
                'value' => 'disabled',
                'code' => $code
            ], 403);
        }

        if ($user->approved === 0) {
            return response()->json([
                'error' => 'Your account has been deactivated. Please contact customer support.',
                'value' => 'deactivated'
            ], 405);
        }

        $credentials = $request->only('username', 'password');

        JWTAuth::factory()->setTTL(60);
        JWTAuth::factory()->setRefreshTTL(20160);

        $params = $request->input();
        if (array_key_exists('isChecked', $params)) {
            $isChecked = $params['isChecked'];
            if ($isChecked) {
                // JWTAuth::factory()->setTTL(2);
                // JWTAuth::factory()->setRefreshTTL(35);
                JWTAuth::factory()->setTTL(129600);
                JWTAuth::factory()->setRefreshTTL(1296000);
            }
        }

        $token = JWTAuth::attempt($credentials);

        return response()->json([
            'success' => true,
            '_t' => $token
        ]);
    }

    public function refreshToken(Request $request)
    {
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();

        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response('token_invalid', 401);
        }

        if (!$payload->hasKey('username')) {
            return response('not_logged_in', 402);
        }

        $user_id = $payload->get('id');

        $user = User::find($user_id);

        $token = JWTAuth::fromUser($user);

        return response()->json([
            'success' => true,
            '_t' => $token
        ]);
    }
}
