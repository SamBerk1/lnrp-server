<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;

use Storage;

use \App\User as User;
use \Tymon\JWTAuth\Facades\JWTAuth;

use \Intervention\Image\ImageManager;

class ImagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getInitialHeroImages($genre_id = NULL, $subgenre_id = NULL)
    {
        $images = app('db')->select("SELECT id FROM latenight.songs WHERE image IS NOT NULL ORDER BY `year` DESC, `score` DESC LIMIT 50");

        return response()->json($images);

    }

    public function getFullHeroImages($genre_id = NULL, $subgenre_id = NULL)
    {
        $images = app('db')->select("SELECT id FROM latenight.songs WHERE image IS NOT NULL ORDER BY `year` DESC, `score` DESC LIMIT 200");

        return response()->json($images);
    }

    public function getTopDownloadImages($genre_id = NULL, $subgenre_id = NULL)
    {
        $images = app('db')->select("SELECT id FROM latenight.songs WHERE image IS NOT NULL ORDER BY `month_downloads` DESC LIMIT 36");

        return response()->json($images);
    }

    public function getImageByID($id = false, $width = 500, $height = 500, $quality = 100)
    {
        if ($id === false) {
            return response()->json([
                'success' => false,
                'reason' => 'No song identification value provided'
            ]);
        }

        $imageManager = new ImageManager(['driver' => 'gd']);

        $release =
            current(
                app('db')->select("SELECT image, zip, genre_id FROM latenight.songs WHERE id = '{$id}'")
            );

        if ($release === false) {
            abort(404, 'No release found for this identifier');
        }

        $release->folder = substr($release->zip, 0, strrpos($release->zip, '/'));

        if (!is_file("{$release->folder}/{$release->image}")) {
            // No image found. Default to genre image_to_swap
            $release_genre =
                current(
                    app('db')->select("SELECT name FROM latenight.songs_genres WHERE id = {$release->genre_id}")
                );

            if ($release_genre !== false) {
                $release_genre = strtolower($release_genre->name);
                $release_genre = str_replace([' '], ['-'], $release_genre);

                // Correction for Reggae/Dancehall
                if ($release_genre == 'reggae/dancehall') {
                    $release_genre = 'reggae';
                }

                // Mask DJ-Edits as Remixes
                if ($release_genre == 'dj-edits') {
                    $release_genre = 'remix';
                }

                $filename = storage_path() . '/app/genre_images/' . $release_genre . '.jpg';

                if (!is_file($filename)) {
                    $filename = storage_path() . '/app/genre_images/default.jpg';
                }
            } else {
                $filename = storage_path() . '/app/genre_images/default.jpg';
	        }
        } else {
            $filename = "{$release->folder}/{$release->image}";
        }

        return $imageManager
                ->make($filename)
                ->resize($width, $width)
                ->response('jpg', $quality);
    }

    public function getNewMusicBackgorund()
    {
        $imageManager = new ImageManager(['driver' => 'gd']);

        return $imageManager->make(storage_path() . '/app/bg-front-new.jpg')->response();
    }

    public function uploadUserImage(Request $request)
    {
        $retoken = false;
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_expired'], 401);
            }
            JWTAuth::setToken($refreshed);
            $payload = JWTAuth::getPayload();
            $retoken = true;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_invalid'], 400);
        }

        if (!$payload->hasKey('username')) {
            return response()->json(['not_logged_in'], 401);
        }

        $user = \App\User::find($payload['id']);

        $extension = $request->file->extension();

        $newFilename = "user_{$user->id}.{$extension}";

        if ($user->image !== "") {
            Storage::delete("user_images/{$user->image}");
        }

        $path = $request->file->storeAs('user_images', $newFilename);

        $user->image = $newFilename;
        $user->save();

        return response()->json([
            'success' => true,
            '_t' => JWTAuth::fromUser($user),
            'user' => $user
        ]);
    }
}
