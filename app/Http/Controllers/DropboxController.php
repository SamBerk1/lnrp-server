<?php
namespace App\Http\Controllers;

use \App\Http\Helpers\DownloadHelper;

use \App\DownloadedRelease;
use \App\DownloadedVersion;
use \App\DropboxState;
use \App\DropboxToken;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

use \App\Http\Middleware\SessionDataStore;

use Kunnu\Dropbox\DropboxApp;
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxFile;
use Kunnu\Dropbox\Exceptions\DropboxClientException;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class DropboxController extends Controller
{
    public $app;
    public $dropbox;
    public $authHelper;
    public $callbackUrl;
    public $authUrl;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $config = [
            'persistent_data_store' => new SessionDataStore
        ];

        // TODO: Move this to environment variables
        $this->app = new DropboxApp(
            env('DROPBOX_OAUTH_CLIENT'),
            env('DROPBOX_OAUTH_SECRET')
        );

        $this->dropbox = new Dropbox($this->app, $config);

        $this->authHelper = $this->dropbox->getAuthHelper();

        $this->callbackUrl = env('DROPBOX_OAUTH_DOMAIN');
    }

    public function connect($user = 0)
    {
        if ($user === 0) {
            abort(404);
        }

        $this->authUrl = $this->authHelper->getAuthUrl($this->callbackUrl);

        $matches = array();

        preg_match('/.*\&state=(?<state>[\w0-9]+)/', $this->authUrl, $matches);

        $state = $matches['state'];

        try {
            $state_info = DropboxState::where('user', '=', $user)->firstOrFail();
            $state_info->state = $state;
            $state_info->used = 0;
            $state_info->save();
        } catch (ModelNotFoundException $e) {
            $dbstate = new DropboxState;
            $dbstate->user = $user;
            $dbstate->state = $state;
            $dbstate->save();
        }

        return view('dropbox-redirect', ['redirect_url' => $this->authUrl]);
    }

    public function auth(Request $request)
    {
        // TODO: set up handler for cancelling the request
        $params = $request->all();

        $accessToken = $this->authHelper->getAccessToken($params['code'], $params['state'], $this->callbackUrl);

        try {
            $state_info = DropboxState::findOrFail($params['state']);

        } catch (ModelNotFoundException $e) {
            abort(404, 'Support information not found');
        }

        if ($state_info->used == 1) {
            abort(400, 'Support information already used');
        }

        $state_info->used = 1;
        $state_info->save();

        try {
            $token_info = DropboxToken::findOrFail($state_info->user);

        } catch (ModelNotFoundException $e) {
            $token_info = new DropboxToken;
            $token_info->user = $state_info->user;
        }

        $token_info->token = $accessToken->getToken();
        $token_info->save();

        return redirect(env('WEB_URL') . '/account/dropbox');
    }

    public function requestDownload($release, $version = false)
    {
        $retoken = false;
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_expired'], 401);
            }
            JWTAuth::setToken($refreshed);
            $payload = JWTAuth::getPayload();
            $retoken = true;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json([
                'success' => false,
                'msg' => 'Your token was expired. Please login again and retry it.'
            ], 401);
        }

        if (!$payload->hasKey('username')) {
            return response()->json([
                'success' => false,
                'msg' => 'Invalid User. Please login and try it again.'
            ], 401);
        }

        $user_id = $payload->get('id');

        try {
            $dropbox_token = DropboxToken::findOrFail($user_id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false,
                'msg' => 'Invalid Dropbox Account. Please link a valid account and retry it.'
            ], 400);
        }

        $download_limit = app('db')->select("SELECT value FROM settings WHERE name = 'download_limit'");
        $download_limit = $download_limit[0]->value;
        $user_limit = app('db')->select("SELECT `limit` FROM download_limits WHERE user = $user_id");
        if ($user_limit) {
            $user_limit = $user_limit[0]->limit;
        }

        // Get Download limits tables Instance
        if ($version) {
            $downloaded = DownloadedVersion::firstOrNew([
                'user' => $user_id,
                'version' => $version,
                'song' => $release
            ]);
        } else {
            $downloaded = DownloadedRelease::firstOrNew([
                'user' => $user_id,
                'song' => $release
            ]);
        }
        $download_count = $downloaded->drop_count + $downloaded->dl_count + $downloaded->crateq_count;

        $isDownload = false;
        if ($download_count) {
            if ($user_limit) {
                if ($download_count >= $user_limit) {
                    $isDownload = true;
                }
            } else {
                if ($download_count >= $download_limit) {
                    $isDownload = true;
                }
            }
        }
        if ($isDownload) {
            return response("We're sorry, but you've already downloaded this song the maximum number of times. If you feel this is an error, please contact us.", 404);
        }
        unset($downloaded);

        if (env('APP_ENV') == 'local') {
            $download_code = 'abcdefghijklmnopqrstuvwxyz0123456789';

            if ($version === false) {
                $filename = 'testing_release.zip';

            } else {
                $filename = 'testing_version.mp3';
            }

        } else {
            $download_code = DownloadHelper::generateDownloadCode($user_id, $release, $version);

            list($file, $filename) = DownloadHelper::generateDownloadFiles($download_code);
        }

        $this->dropbox->setAccessToken($dropbox_token->token);

        try {
            $file_info = $this->dropbox->saveUrl("/{$filename}", env('DROPBOX_DOWNLOAD_DOMAIN') . "/download/{$download_code->code}/dropbox", ['autorename' => true]);
        } catch (DropboxClientException $e) {
            return response()->json([
                'success' => false,
                'msg' => $e->getMessage()
            ], 500);
        }

        if ($retoken) {
            $user = User::find($user_id);
            $token = JWTAuth::fromUser($user);
            return response()->json([
                'success' => true,
                'msg' => 'Your request has been successfully added to your Dropbox account',
                '_t' => $token
            ]);
        } else {
            return response()->json([
                'success' => true,
                'msg' => 'Your request has been successfully added to your Dropbox account'
            ], 200);
        }
    }
}
