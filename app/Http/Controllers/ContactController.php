<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use \Illuminate\Http\Request;
use \Illuminate\Support\Facades\Mail;

use App\Mail\ContactForm;

class ContactController extends Controller
{
    public function __construct()
    {
        //
    }

    public function sendContactForm(Request $request)
    {
        Mail::to(env('CONTACT_EMAIL'))
            ->send(new ContactForm($request->input()));

        return response()->json([
            'success' => true
        ]);
    }
}
