<?php

namespace App\Http\Controllers;

use App\User as User;
use \App\DownloadedRelease;
use \App\DownloadedVersion;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

use \Intervention\Image\ImageManager;

class ReleasesController extends Controller
{
    // Previews are limited to 30 seconds
    private $preview_length = 30;

    private $local_preview = [
        '/var/drive_b/test_release/Drake - Pop Style (Dirty).mp3',
        '/var/drive_b/test2/test.mp3',
        '/var/drive_b/test3/test.mp3'
    ];

    public function __construct()
    {
        //
    }

    public function getAllReleasesCount()
    {
        $releases =
            current(
                app('db')->select("SELECT COUNT(*) AS `total` FROM latenight.songs")
            );

        return response()->json($releases);
    }

    public function getCurrentlyPlaying($limit = 10)
    {
        $releases = app('db')->select("SELECT s.id, s.artist, s.title, s.label FROM latenight.currently_playing cp LEFT JOIN latenight.songs s ON cp.release = s.id ORDER BY cp.created_at DESC LIMIT 0, {$limit}");

        return response()->json($releases);

    }

    public function getReleaseInfo($id)
    {
        $logged_in = true;
        $retoken = false;

        // Two ways to prove if you're not logged in
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_expired'], 401);
            }
            JWTAuth::setToken($refreshed);
            $payload = JWTAuth::getPayload();
            $retoken = true;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            // #1 Invalid or no JWT sent on the request
            $logged_in = false;
        }

        if ($logged_in == true && !$payload->hasKey('username')) {
            // #2 No username in JWT == malformed or malicious JWT
            $logged_in = false;
        }

        $info = app('db')->select(
            'SELECT s.artist, s.title, sv.type, sv.time, sv.filename '.
            'FROM latenight.songs s '.
            'LEFT JOIN latenight.songs_versions sv ON s.id = sv.song '.
            "WHERE s.id = {$id} ".
            'LIMIT 0, 1'
        );

        $info = current($info);

        $filename = $info->filename;
        $imagename = str_replace('mp3', 'png', $filename);

        $imageManager = new ImageManager(['driver' => 'gd']);

        if (!is_file($imagename)) {
            $imagename = env('DRIVE_B_FOLDER') . "/default.jpg";
        }

        $info->wave = $imageManager->make($imagename)->encode('data-url')->encoded;

        unset($info->filename);

        if (env('APP_ENV') == 'local') {
            // Hard code a response for localized testing
            $info->time = 210;
        } else {
            list($minutes, $seconds) = explode(':', $info->time);
            $info->time = ($minutes * 60) + $seconds;
        }

        if (!$logged_in) {
            // If not logged in, override all time to preview time
            $info->time = $this->preview_length;
        }

        $response = (array)$info;
        if ($retoken) {
            $user_id = $payload->get('id');
            $user = User::find($user_id);
            $token = JWTAuth::fromUser($user);
            $response['_t'] = $token;
        }

        return response()->json($response);
    }

    public function getReleaseDetail($id)
    {
        $search_query =
            'SELECT s.id, s.remix, s.artist, s.title, from_unixtime(s.date, "%b %e, %Y") AS `added`, sv.time, s.year, s.label, s.throwback, s.certified, s.comments, s.score, s.ratings, '.
            'MAX(sv.bpm) AS `max_bpm`, '.
            'IF (s.subgenre_id != 0, CONCAT(g.name, " | ", sg.name), g.name) AS `genre`, '.
            'IFNULL(DATE_FORMAT(s.updated, "%b %e, %Y"), "false") AS `updated`, '.
            'IF(MIN(sv.bpm) != MAX(sv.bpm), CONCAT(MIN(sv.bpm), "-", MAX(sv.bpm)), MAX(sv.bpm)) AS `bpm` '.
            'FROM songs s '.
            'LEFT JOIN songs_versions sv ON s.id = sv.song '.
            'LEFT JOIN songs_genres g ON s.genre_id = g.id '.
            'LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id '.
            "WHERE s.id = {$id} ";

        $detail = current(
            app('db')->select($search_query)
        );

        if ($detail->ratings > 0) {
            $detail->rating = round($detail->score / $detail->ratings, 1);
        } else {
            $detail->rating = 0;
        }

        // Grab all versions
        $detail->versions =
            app('db')->select(
                'SELECT id, type, `time` '.
                'FROM songs_versions '.
                "WHERE song = {$detail->id} ".
                'ORDER BY `sort` ASC'
            );

        if ($detail->remix) {
            $detail->remixes = $this->getDetail($detail->remix);
        }

        return response()->json($detail);
    }

    public function getVersionInfo($id)
    {
        $logged_in = true;
        $retoken = false;

        // Two ways to prove if you're not logged in
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_expired'], 401);
            }
            JWTAuth::setToken($refreshed);
            $payload = JWTAuth::getPayload();
            $retoken = true;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            // #1 Invalid or no JWT sent on the request
            $logged_in = false;
        }

        if ($logged_in == true && !$payload->hasKey('username')) {
            // #2 No username in JWT == malformed or malicious JWT
            $logged_in = false;
        }

        $info = app('db')->select(
            'SELECT s.artist, s.title, sv.type, sv.time, sv.filename '.
            'FROM latenight.songs s '.
            'LEFT JOIN latenight.songs_versions sv ON s.id = sv.song '.
            "WHERE sv.id = {$id} ".
            'LIMIT 0, 1'
        );

        $info = current($info);

        $filename = $info->filename;
        $imagename = str_replace('mp3', 'png', $filename);

        $imageManager = new ImageManager(['driver' => 'gd']);

        if (!is_file($imagename)) {
            $imagename = env('DRIVE_B_FOLDER') . "/default.jpg";
        }

        $info->wave = $imageManager->make($imagename)->encode('data-url')->encoded;

        unset($info->filename);

        if (env('APP_ENV') == 'local') {
            // Hard code a response for localized testing
            $info->time = 210;

        } else {
            list($minutes, $seconds) = explode(':', $info->time);

            $info->time = ($minutes * 60) + $seconds;
        }

        if (!$logged_in) {
            // If not logged in, override all time to preview time
            $info->time = $this->preview_length;
        }

        $response = (array)$info;
        if ($retoken) {
            $user_id = $payload->get('id');
            $user = User::find($user_id);
            $token = JWTAuth::fromUser($user);
            $response['_t'] = $token;
        }

        return response()->json($response);
    }

    public function playRelease($id)
    {
        $logged_in = true;

        if (strlen($id) > 6) {
            // ID is special version meant to load preview tracks
            // Need to convert
            $logged_in = false;

            $oldId = $id;
            $id = (100000000 - $oldId) / ((int) (gmdate('w')) + 1);
        }

        $info = app('db')->select(
            'SELECT s.artist, s.title, sv.type, sv.time, sv.filename '.
            'FROM latenight.songs s '.
            'LEFT JOIN latenight.songs_versions sv ON s.id = sv.song '.
            "WHERE s.id = {$id} ".
            'LIMIT 0, 1'
        );

        $info = current($info);

        $filename = $info->filename;
        $time = $info->time;

        $song = $id;

        $handle = fopen($filename, 'rb');
        // $content = fread($handle, filesize($filename));
        $content = stream_get_contents($handle);
        fclose($handle);

        $audio_length = strlen($content);

        // If the user isn't logged in, truncate down to preview
        if (!$logged_in) {
            // Get the total number of seconds in the song
            list($t_min, $t_sec) = explode(':', $time);
            $time = ($t_min * 60) + $t_sec;

            // Calculate the length of the song to return for the preview
            $audio_length = round((strlen($content) * $this->preview_length) / $time);

            $content = substr($content, strlen($content) / 3, $audio_length);

            // Log the play from a registered user
            app('db')->insert("INSERT INTO `latenight`.`currently_playing` (`release`) VALUES ('{$song}')");
        }

        return response($content)
                ->header('Accept-Ranges', 'bytes')
                ->header('Content-Type', 'audio/mp3')
                ->header('Content-Length', $audio_length);
        // return response()->file($filename, [
        //     'Content-Type' => 'audio/mp3',
        //     'Content-Length' => filesize($filename)
        // ]);
    }

    public function waveRelease($id = false, $width = 1800, $height = 280, $quality = 90)
    {

        if ($id === false) {
            return response()->json([
                'success' => false,
                'reason' => 'No song identification value provided'
            ]);
        }

        $logged_in = true;

        if (strlen($id) > 6) {
            // ID is special version meant to load preview tracks
            // Need to convert
            $logged_in = false;

            $oldId = $id;
            $id = (100000000 - $oldId) / ((int) (gmdate('w')) + 1);
        }

        $info = app('db')->select(
            'SELECT s.artist, s.title, sv.type, sv.time, sv.filename '.
            'FROM latenight.songs s '.
            'LEFT JOIN latenight.songs_versions sv ON s.id = sv.song '.
            "WHERE s.id = {$id} ".
            'LIMIT 0, 1'
        );

        $info = current($info);

        $filename = $info->filename;
        $imagename = str_replace('mp3', 'png', $filename);

        $imageManager = new ImageManager(['driver' => 'gd']);

        if (!is_file($imagename)) {
            $imagename = env('DRIVE_B_FOLDER') . "/default.jpg";
        }

        return $imageManager
            ->make($imagename)
            ->resize($width, $height)
            ->response('png', $quality);
    }

    public function playVersion($id)
    {
        $logged_in = true;

        if (strlen($id) > 6) {
            // ID is special version meant to load preview tracks
            // Need to convert
            $logged_in = false;

            $oldId = $id;
            $id = (100000000 - $oldId) / ((int) (gmdate('w')) + 1);
        }

        $info = app('db')->select(
            'SELECT s.id AS `song_id`, s.artist, s.title, sv.type, sv.time, sv.filename '.
            'FROM latenight.songs s '.
            'LEFT JOIN latenight.songs_versions sv ON s.id = sv.song '.
            "WHERE sv.id = {$id} ".
            'LIMIT 0, 1'
        );

        $info = current($info);
        $time = $info->time;

        $song = $info->song_id;

        $filename = $info->filename;


        $handle = fopen($filename, 'rb');
        // $content = fread($handle, filesize($filename));
        $content = stream_get_contents($handle);
        fclose($handle);

        $audio_length = strlen($content);

        // Get the total number of seconds in the song
        list($t_min, $t_sec) = explode(':', $time);
        $time = ($t_min * 60) + $t_sec;

        // If the user isn't logged in, truncate down to preview
        if (!$logged_in) {

            // Calculate the length of the song to return for the preview
            $audio_length = round((strlen($content) * $this->preview_length) / $time);

            $content = substr($content, strlen($content) / 3, $audio_length);

            // Log the play from a registered user
            app('db')->insert("INSERT INTO `latenight`.`currently_playing` (`release`) VALUES ('{$song}')");
        } else {
            $audio_length = floor(strlen($content) / $time) * ($time - 1);

            $content = substr($content, 0, $audio_length);
        }

        $filesize = strlen($content);

        return response($content)
                ->header('Accept-Ranges', 'bytes')
                ->header('Content-Type', 'audio/mp3')
                ->header('Content-Length', $filesize);

        // return response()->file($filename, [
        //     'Content-Type' => 'audio/mp3',
        //     'Content-Length' => filesize($filename)
        // ]);
    }

    public function testPlayVersion($id)
    {
        $logged_in = true;

        if (strlen($id) > 6) {
            // ID is special version meant to load preview tracks
            // Need to convert
            $logged_in = false;

            $oldId = $id;
            $id = (100000000 - $oldId) / ((int) (gmdate('w')) + 1);
        }

        $info = app('db')->select(
            'SELECT s.id AS `song_id`, s.artist, s.title, sv.type, sv.time, sv.filename '.
            'FROM latenight.songs s '.
            'LEFT JOIN latenight.songs_versions sv ON s.id = sv.song '.
            "WHERE sv.id = {$id} ".
            'LIMIT 0, 1'
        );

        $info = current($info);
        $time = $info->time;

        $song = $info->song_id;

        $filename = $info->filename;


        $handle = fopen($filename, 'rb');
        // $content = fread($handle, filesize($filename));
        $content = stream_get_contents($handle);
        fclose($handle);

        $audio_length = strlen($content);
        $filesize = strlen($content);

        // Get the total number of seconds in the song
        list($t_min, $t_sec) = explode(':', $time);
        $time = ($t_min * 60) + $t_sec;

        // If the user isn't logged in, truncate down to preview
        if (!$logged_in) {

            // Calculate the length of the song to return for the preview
            $audio_length = round((strlen($content) * $this->preview_length) / $time);

            $content = substr($content, strlen($content) / 3, $audio_length);

            // Log the play from a registered user
            app('db')->insert("INSERT INTO `latenight`.`currently_playing` (`release`) VALUES ('{$song}')");
        } else {
            $audio_length = floor(strlen($content) / $time) * $time;

            $content = substr($content, 0, $audio_length);
        }

        $return = [
            'calc' => $audio_length,
            'time' => $time,
            'length' => $filesize,
            'res' => strlen($content)
        ];

        return response()->json($return);
    }

    public function waveVersion($id = false, $width = 1800, $height = 280, $quality = 90)
    {

        if ($id === false) {
            return response()->json([
                'success' => false,
                'reason' => 'No song identification value provided'
            ]);
        }

        $logged_in = true;

        if (strlen($id) > 6) {
            // ID is special version meant to load preview tracks
            // Need to convert
            $logged_in = false;

            $oldId = $id;
            $id = (100000000 - $oldId) / ((int) (gmdate('w')) + 1);
        }

        $info = app('db')->select(
            'SELECT s.id AS `song_id`, s.artist, s.title, sv.type, sv.time, sv.filename '.
            'FROM latenight.songs s '.
            'LEFT JOIN latenight.songs_versions sv ON s.id = sv.song '.
            "WHERE sv.id = {$id} ".
            'LIMIT 0, 1'
        );

        $info = current($info);

        $filename = $info->filename;
        $imagename = str_replace('mp3', 'png', $filename);

        $imageManager = new ImageManager(['driver' => 'gd']);

        if (!is_file($imagename)) {
            $imagename = env('DRIVE_B_FOLDER') . "/default.jpg";
        }

        return $imageManager
            ->make($imagename)
            ->resize($width, $height)
            ->response('png', $quality);
    }

    public function getReleases(Request $request, $start, $limit) {
        if (!(is_numeric($start) && is_numeric($limit))) {
            return response('Invalid request', 400);
        }

        $where = array();

        $filter = new ReleaseSearchFilter($request->input());

        $search_query =
            'SELECT IF(DATEDIFF(NOW(), s.updated) <= 5, 1, 2) AS `priority`, '.
            's.id, s.remix, s.artist, s.title, from_unixtime(s.date, "%b %e, %Y") AS `added`, sv.time, s.year, s.label, s.throwback, s.certified, s.comments, s.score, s.ratings, '.
            'MAX(sv.bpm) AS `max_bpm`, IFNULL(DATE_FORMAT(s.updated, "%b %e, %Y"), "false") AS `updated`, '.
            'IF (s.subgenre_id != 0, CONCAT(g.name, " | ", sg.name), g.name) AS `genre`, '.
            'IF(MIN(sv.bpm) != MAX(sv.bpm), CONCAT(MIN(sv.bpm), "-", MAX(sv.bpm)), MAX(sv.bpm)) AS `bpm` '.
            'FROM songs s '.
            'LEFT JOIN songs_versions sv ON s.id = sv.song '.
            'LEFT JOIN songs_genres g ON s.genre_id = g.id '.
            'LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id ';

        if (count($filter->genre_id) > 0 || count($filter->subgenre_id) > 0) {
            $or = array();

            if (count($filter->genre_id) > 0) {
                array_push(
                    $or,
                    's.genre_id IN (' . implode(',', $filter->genre_id) . ')'
                );
            }

            if (count($filter->subgenre_id) > 0) {
                array_push(
                    $or,
                    's.subgenre_id IN (' . implode(',', $filter->subgenre_id) . ')'
                );
            }

            array_push(
                $where,
                '(' . implode(' OR ', $or). ') '
            );

            unset($or);
        }

        if (count($filter->label) > 0) {
            $or = array();

            foreach ($filter->label as $l) {
                array_push($or, "s.label LIKE '{$l}%'");
            }

            array_push(
                $where,
                '(' . implode(' OR ', $or) . ') '
            );

            unset($or);
        }

        if (count($filter->letter) > 0) {
            $or = array();

            foreach ($filter->letter as $l) {
                array_push($or, "s.artist LIKE '{$l}%'");
            }

            array_push(
                $where,
                '(' . implode(' OR ', $or) . ') '
            );

            unset($or);
        }

        if (count($filter->decade) > 0) {
            $or = array();

            foreach ($filter->decade as $d) {
                $decade_min = (int) $d;
                $decade_max = $decade_min + 9;

                array_push($or, "s.year BETWEEN {$decade_min} AND {$decade_max}");

                unset($decade_min, $decade_max);
            }

            array_push(
                $where,
                '(' . implode(' OR ', $or) . ') '
            );

            unset($or);
        }

        // Year range Query
        if (!is_null($filter->minYear)) {
            $or = array();
            $yearMin = (int) $filter->minYear;
            array_push($or, "s.year >= {$yearMin}");
            array_push(
                $where,
                '(' . implode(' OR ', $or) . ') '
            );
            unset($or);
        }

        if (!is_null($filter->maxYear)) {
            $or = array();
            $yearMax = (int) $filter->maxYear;
            array_push($or, "s.year <= {$yearMax}");
            array_push(
                $where,
                '(' . implode(' OR ', $or) . ') '
            );
            unset($or);
        }

        if (count($filter->version) > 0) {
            array_push(
                $where,
                'LOWER(sv.type) IN ("' . implode('","', $filter->version) . '") '
            );
        }

        if (count($filter->bpm) > 0) {
            switch ($filter->bpm) {
                case 'less_80':
                    array_push(
                        $where,
                        'sv.bpm <= 80 '
                    );
                    break;

                case '80_100':
                    array_push(
                        $where,
                        'sv.bpm BETWEEN 80 AND 100 '
                    );
                    break;

                case '100_140':
                    array_push(
                        $where,
                        'sv.bpm BETWEEN 100 AND 140 '
                    );
                    break;

                case '140_great':
                    array_push(
                        $where,
                        'sv.bpm >= 140 '
                    );
                    break;
            }
        }

        if (count($filter->date) > 0) {
            foreach ($filter->date as $d) {
                array_push(
                    $where,
                    "s.date >= UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL {$d})) "
                );
            }
        }

        if (!is_null($filter->throwback)) {
            array_push(
                $where,
                's.throwback = ' . ((bool) $filter->throwback ? 1 : 0) . ' '
            );
        }

        if (!is_null($filter->remix)) {
            array_push(
                $where,
                's.remix ' . ((bool) $filter->remix ? '!=' : '=') . ' 0 '
            );
        }

        if (count($filter->search) > 0) {

            $all_where = "";
            $returned = array();

            for ($i = 0; $i < count($filter->search); $i++) {

                // echo $filter->search[$i].':';
                // Addslashes is safer handling of special strings that have character that can break a query
                $searchLists = addslashes($filter->search[$i]);
                $searchLists = str_replace(" "," +",$searchLists);

                    $query_where = "((MATCH(artist, title) AGAINST('+{$searchLists}' IN BOOLEAN MODE)))";
                    
                    if ($i == 0) {
                        $all_where = '('.$query_where.')';
                    } else {
                        $all_where = $all_where . ' OR ' . '('.$query_where.')';
                    }

            }

            $res_query = "SELECT s.id as `id` FROM songs s Where {$all_where}";
            
            $res_ids = app('db')->select($res_query);

            $query_ids = array();
            if (count($res_ids)>0) {
                foreach ($res_ids as $d => $id_val) {
                    if ($id_val->id) {
                        array_push($query_ids, $id_val->id);
                    }
                }

                $q_ids = implode($query_ids, ',');
                array_push(
                    $where,
                    "s.id IN ({$q_ids}) "
                );
            }

            $total_count = isset($query_ids) ? count($query_ids) : 0;

            // $return = [
            //     'search' => $filter->search,
            //     'ids' => $query_ids,
            //     'query' => $res_query
            // ];
    
            // return response()->json($return);
        }

        if (count($where) > 0) {
            $search_query .= 'WHERE ' . implode('AND ', $where) . ' ';
        }

        $search_query .=
            'GROUP BY s.id ';

        // Only need this much for a count. Get the total.
        if (count($filter->search) == 0) {
            $releases_count = current(
                app('db')->select("SELECT COUNT(*) AS `total` FROM ({$search_query}) AS `query`")
            );
            $total_count = $releases_count->total;
        }

        if ($filter->sort !== false) {
            switch ($filter->sort) {
                case 'artist':
                    $search_query .= 'ORDER BY s.artist ASC ';
                    break;

                case 'title':
                    $search_query .= 'ORDER BY s.title ASC ';
                    break;

                case 'bpm':
                    $search_query .= 'ORDER BY `max_bpm` DESC ';
                    break;

                case 'bpm_slow':
                    $search_query .= 'ORDER BY `max_bpm` ASC ';
                    break;

                case 'rating':
                    $search_query .= 'ORDER BY (s.score / s.ratings) DESC ';
                    break;

                case 'votes':
                    $search_query .= 'ORDER BY s.ratings DESC ';
                    break;

                case 'year_new':
                    $search_query .= 'ORDER BY s.year DESC ';
                    break;

                case 'year_old':
                    $search_query .= 'ORDER BY s.year ASC ';
                    break;

                case 'date':
                    $search_query .= 'ORDER BY s.date DESC ';
                    break;

                case 'date_old':
                    $search_query .= 'ORDER BY s.date ASC ';
                    break;
            }

        } else {
            $search_query .= 'ORDER BY `priority` ASC, s.date DESC ';
        }

        $search_query .=
            "LIMIT {$start}, {$limit}";

        $releases = [];

        if ($total_count > 0) {
            $releases = app('db')->select($search_query);
        }

        foreach ($releases as &$r) {
            // Calculate rating value
            if ($r->ratings == 0) {
                // If no ratings yet, default to 0
                $r->rating = 0;

            } else {
                $r->rating = round($r->score / $r->ratings, 1);
            }

            // Grab all versions
            $r->versions =
                app('db')->select(
                    'SELECT id, type, `time` '.
                    'FROM songs_versions '.
                    "WHERE song = {$r->id} ".
                    'ORDER BY `sort` ASC'
                );
            if ($r->remix > 0) {
                $r->remixes = $this->getDetail($r->remix);
            }
        }

        // $return = [
        //     'total' => count($query_ids),
        //     'releases' => $releases
        // ];

        $return = [
            'total' => $total_count,
            'releases' => $releases
        ];

        if (env('APP_ENV') == 'local') {
            $return['query'] = $search_query;
        }

        return response()->json($return);
    }

    public function getWeekTopDownloads($limit = 25) {

        $top_query = "SELECT `song`, COUNT(*) as cnt FROM `download_log` WHERE (`type` = 'song' OR `type` = 'zip') AND `song` > 0 AND `downloaded` >= DATE(NOW()) + INTERVAL -7 DAY GROUP BY `song` ORDER BY cnt DESC LIMIT {$limit}";
        $songs = app('db')->select($top_query);
        
        $ids = array();
        foreach ($songs as $song) {
            array_push($ids, $song->song);
        }

        $search_query =
            'SELECT s.id, s.remix, s.artist, s.title, from_unixtime(s.date, "%b %e, %Y") AS `added`, sv.time, s.year, s.label, s.throwback, s.certified, s.comments, s.score, s.ratings, '.
            'MAX(sv.bpm) AS `max_bpm`, '.
            'IF (s.subgenre_id != 0, CONCAT(g.name, " | ", sg.name), g.name) AS `genre`, '.
            'IFNULL(DATE_FORMAT(s.updated, "%b %e, %Y"), "false") AS `updated`, '.
            'IF(MIN(sv.bpm) != MAX(sv.bpm), CONCAT(MIN(sv.bpm), "-", MAX(sv.bpm)), MAX(sv.bpm)) AS `bpm` '.
            'FROM songs s '.
            'LEFT JOIN songs_versions sv ON s.id = sv.song '.
            'LEFT JOIN songs_genres g ON s.genre_id = g.id '.
            'LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id '.
            "WHERE s.id IN (" . implode(',', $ids) . ") GROUP BY s.id ORDER BY FIELD (s.id, " . implode(',', $ids) . ")";

        $releases = app('db')->select($search_query);

        foreach ($releases as &$r) {
            // Calculate rating value
            if ($r->ratings == 0) {
                // If no ratings yet, default to 0
                $r->rating = 0;
            } else {
                $r->rating = round($r->score / $r->ratings, 1);
            }

            // Grab all versions
            $r->versions =
                app('db')->select(
                    'SELECT id, type, `time` '.
                    'FROM songs_versions '.
                    "WHERE song = {$r->id} ".
                    'ORDER BY `sort` ASC'
                );

            if ($r->remix > 0) {
                $r->remixes = $this->getDetail($r->remix);
            }
        }

        $return = [
            'releases' => $releases
        ];

        return response()->json($return);
    
    }

    public function rateRelease($id, $rating)
    {
        $retoken = false;
        try {
            JWTAuth::parseToken();
            $payload = JWTAuth::getPayload();
        } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
            try {
                $refreshed = JWTAuth::refresh();
            } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
                return response()->json(['token_expired'], 401);
            }
            JWTAuth::setToken($refreshed);
            $payload = JWTAuth::getPayload();
            $retoken = true;
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
            return response()->json(['token_invalid'], 400);
        }

        if (!$payload->hasKey('username')) {
            return response()->json(['not_logged_in'], 401);
        }

        app('db')->update(
            'UPDATE songs '.
            'SET `ratings` = `ratings` + 1, '.
            "`score` = `score` + {$rating} ".
            "WHERE `id` = {$id}"
        );

        $rating =
            current(
                app('db')->select(
                    'SELECT id, ratings, score '.
                    'FROM songs '.
                    "WHERE id = {$id}"
                )
            );

        // Calculate rating value
        $rating->rating = round($rating->score / $rating->ratings, 1);

        $response = (array)$rating;

        $user_id = $payload->get('id');
        $user = User::find($user_id);
        if ($retoken) {
            $token = JWTAuth::fromUser($user);
            $response['_t'] = $token;
        }

        // Get the downloads of the version & release
        $version_download = DownloadedVersion::where('user', $user_id)->sum('crateq_count');
        $release_download = DownloadedRelease::where('user', $user_id)->sum('crateq_count');
        $response['crateq'] = $release_download + $version_download;

        return response()->json($response);
    }

    public function getTopDownloads($genre = false, $subgenre = false)
    {
        // Generate query first looking without throwbacks
        list($search_query, $name_query) = $this->generateTopDownloadQueries($genre, $subgenre);

        // Run the month-download query
        $releases = app('db')->select($search_query);

        // If the query didn't return anything, re-run looking with throwbacks
        if (count($releases) === 0) {
            list($search_query, $name_query) = $this->generateTopDownloadQueries($genre, $subgenre, false);

            $releases = app('db')->select($search_query);
        }

        // If the query STILL didn't return anything. Throw back an error.
        if (count($releases) === 0) {
            return response('No releases found', 404);
        }

        $names = current(
            app('db')->select($name_query)
        );

        return response()->json([
            'genre' => $genre !== false ? $names->genre : false,
            'subgenre' => $subgenre !== false ? $names->subgenre : false,
            'releases' => $releases
        ]);
    }

    public function getTopDownloadedRelease()
    {
        $search_query =
            'SELECT s.id, s.remix, s.artist, s.title, s.year, s.label, s.throwback, s.certified, '.
            'IF (s.subgenre_id != 0, CONCAT(g.name, " | ", sg.name), g.name) AS `genre` '.
            'FROM songs s '.
            'LEFT JOIN songs_genres g ON s.genre_id = g.id '.
            'LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id '.
            'GROUP BY s.id '.
            'ORDER BY s.month_downloads DESC '.
            'LIMIT 1';

        $release = app('db')->select($search_query);

        return response()->json(current(
            $release
        ));
    }

    public function getReleaseMonths()
    {
        $query =
            'SELECT '.
                'DATE_FORMAT(FROM_UNIXTIME(date), "%M %Y") AS `format`, '.
                'MONTH(FROM_UNIXTIME(date)) AS `month`, '.
                'YEAR(FROM_UNIXTIME(date)) AS `year` '.
            'FROM latenight.songs '.
            'GROUP BY '.
                'YEAR(FROM_UNIXTIME(date)), '.
                'MONTH(FROM_UNIXTIME(date)) '.
            'ORDER BY '.
                '`year` DESC, `month` DESC';

        $months = app('db')->select($query);

        return response()->json($months);
    }

    public function getNewReleases($month = false, $year = false)
    {
        // $mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");

        $ordered_releases = array();

        // If not month/year provided, default to current
        if ($month === false || $year === false) {
            $month = date('m');
            $year = date('Y');
        }

        $month_name = $month;
        if ($month < 10) {
            $month_name = '0'.$month;
        }
        // $month_name = $mons[$month];

        // Comparison for updated priority is within 5 days of the month
        // Get comparison day total
        if ( date('j') <= 5 || date('j') - 5 < 1 )
            $date_diff = date('j');
        else
            $date_diff = 5;

        $search_query =
            "SELECT IF(DATEDIFF(NOW(), s.updated) <= 5, 1, 2) AS `priority`, ".
            's.date, s.id, s.remix, s.artist, s.title, from_unixtime(s.date, "%b %e, %Y") AS `added`, sv.time, s.year, s.label, s.throwback, s.certified, s.comments, s.score, s.ratings, '.
            'MAX(sv.bpm) AS `max_bpm`, IFNULL(DATE_FORMAT(s.updated, "%b %e, %Y"), "false") AS `updated`, '.
            'IF (s.subgenre_id != 0, CONCAT(g.name, " | ", sg.name), g.name) AS `genre`, '.
            'IF(MIN(sv.bpm) != MAX(sv.bpm), CONCAT(MIN(sv.bpm), "-", MAX(sv.bpm)), MAX(sv.bpm)) AS `bpm` '.
            'FROM songs s '.
            'LEFT JOIN songs_versions sv ON s.id = sv.song '.
            'LEFT JOIN songs_genres g ON s.genre_id = g.id '.
            'LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id '.
            "WHERE date BETWEEN UNIX_TIMESTAMP(STR_TO_DATE('{$month},01,{$year}', '%m,%d,%Y')) AND (UNIX_TIMESTAMP(DATE(LAST_DAY(STR_TO_DATE('{$month},{$year}', '%m,%Y')))) + 86399) ".
            "OR (LEFT(s.updated, 7) = '{$year}-{$month_name}')".
            'GROUP BY s.id '.
            'ORDER BY `priority`, CASE `priority` WHEN 1 THEN s.updated ELSE date END DESC';

        $releases_count = current(
            app('db')->select("SELECT COUNT(*) AS `total` FROM ({$search_query}) AS `query`")
        );

        $releases = app('db')->select($search_query);

        // $return = [
        //     'query' => $search_query,
        //     'count' => $releases_count,
        //     'release' => $releases
        // ];

        // return response()->json($return);

        
        $current_month = '';

        foreach ($releases as &$r) {
            // Calculate rating value
            if ($r->ratings == 0) {
                // If no ratings yet, default to 0
                $r->rating = 0;

            } else {
                $r->rating = round($r->score / $r->ratings, 1);
            }

            // Grab all versions
            $r->versions =
                app('db')->select(
                    'SELECT id, type, `time` '.
                    'FROM songs_versions '.
                    "WHERE song = {$r->id} ".
                    'ORDER BY `sort` ASC'
                );

            if ($r->remix > 0) {
                $r->remixes = $this->getDetail($r->remix);
            }

            $comparison_date = $r->updated !== 'false' ? strtotime($r->updated) : $r->date;

            if (($r->updated !== 'false' && $r->priority == 1 ? 'Updated ' : '') . date('F d, Y', $comparison_date) !== $current_month) {
                $current_month = ($r->updated !== 'false' && $r->priority == 1 ? 'Updated ' : '') . date('F d, Y', $comparison_date);
                $ordered_releases[$current_month] = array(
                    'title' => $current_month,
                    'releases' => array()
                );
            }

            array_push(
                $ordered_releases[$current_month]['releases'],
                $r
            );
        }

        $return = [
            'total' => $releases_count->total,
            'groups' => array_values($ordered_releases)
        ];

        return response()->json($return);
    }

    private function getDetail($id)
    {
        $search_query =
            'SELECT s.id, s.remix, s.artist, s.title, from_unixtime(s.date, "%b %e, %Y") AS `added`, sv.time, s.year, s.label, s.throwback, s.certified, s.comments, s.score, s.ratings, '.
            'MAX(sv.bpm) AS `max_bpm`, '.
            'IF (s.subgenre_id != 0, CONCAT(g.name, " | ", sg.name), g.name) AS `genre`, '.
            'IFNULL(DATE_FORMAT(s.updated, "%b %e, %Y"), "false") AS `updated`, '.
            'IF(MIN(sv.bpm) != MAX(sv.bpm), CONCAT(MIN(sv.bpm), "-", MAX(sv.bpm)), MAX(sv.bpm)) AS `bpm` '.
            'FROM songs s '.
            'LEFT JOIN songs_versions sv ON s.id = sv.song '.
            'LEFT JOIN songs_genres g ON s.genre_id = g.id '.
            'LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id '.
            "WHERE s.id = {$id} ";

        $detail = current(
            app('db')->select($search_query)
        );

        if (empty($detail->id)) {
            return null;
        }

        if ($detail->ratings > 0) {
            $detail->rating = round($detail->score / $detail->ratings, 1);
        } else {
            $detail->rating = 0;
        }

        // Grab all versions
        $detail->versions =
            app('db')->select(
                'SELECT id, type, `time` '.
                'FROM songs_versions '.
                "WHERE song = {$detail->id} ".
                'ORDER BY `sort` ASC'
            );

        return $detail;
    }

    private function generateTopDownloadQueries($genre = false, $subgenre = false, $throwback = false)
    {
        $where = array();

        $search_query =
            'SELECT s.id, s.remix, s.artist, s.title, s.year, s.label, s.throwback, s.certified, '.
            'IF (s.subgenre_id != 0, CONCAT(g.name, " | ", sg.name), g.name) AS `genre` '.
            'FROM songs s '.
            'LEFT JOIN songs_genres g ON s.genre_id = g.id '.
            'LEFT JOIN songs_genres_sub sg ON s.subgenre_id = sg.id ';

        if ($throwback) {
            array_push(
                $where,
                "s.throwback = 0"
            );
        }

        if ($genre !== false) {
            array_push(
                $where,
                "s.genre_id = {$genre}"
            );
        }

        if ($subgenre !== false) {
            array_push(
                $where,
                "s.subgenre_id = {$subgenre}"
            );
        }

        if (count($where) > 0) {
            $search_query .= 'WHERE ' . implode(' AND ', $where) . ' ';
        }

        $search_query .=
            'GROUP BY s.id '.
            'ORDER BY s.month_downloads DESC '.
            'LIMIT 25';

        $name_query =
            'SELECT g.name AS `genre`, sg.name AS `subgenre` '.
            'FROM latenight.songs s '.
            'JOIN latenight.songs_genres g ON s.genre_id = g.id '.
            'LEFT JOIN latenight.songs_genres_sub sg ON s.`subgenre_id` = sg.id ';

        if (count($where) > 0) {
            $name_query .= 'WHERE ' . implode(' AND ', $where);
        }

        $name_query .= ' LIMIT 1';

        return [$search_query, $name_query];
    }
}

class ReleaseSearchFilter {
    public $genre_id;
    public $subgenre_id;
    public $label;
    public $letter;
    public $decade;
    public $version;
    public $bpm;
    public $date;
    public $minYear;
    public $maxYear;
    public $throwback;
    public $remix;
    public $search;

    public $sort;

    public function __construct($obj)
    {
        $this->genre_id = isset($obj['genre_id']) && is_array($obj['genre_id']) ?
            $obj['genre_id']:
            [];

        $this->subgenre_id = isset($obj['subgenre_id']) && is_array($obj['subgenre_id']) ?
            $obj['subgenre_id']:
            [];

        $this->label = isset($obj['label']) && is_array($obj['label']) ?
            $obj['label']:
            [];

        $this->letter = isset($obj['letter']) && is_array($obj['letter']) ?
            $obj['letter']:
            [];

        $this->decade = isset($obj['decade']) && is_array($obj['decade']) ?
            $obj['decade']:
            [];

        $this->minYear = isset($obj['minYear']) ?
            $obj['minYear']:
            null;

        $this->maxYear = isset($obj['maxYear']) ?
            $obj['maxYear']:
            null;

        $this->version = isset($obj['version']) && is_array($obj['version']) ?
            $obj['version']:
            [];

        $this->bpm = isset($obj['bpm']) && is_array($obj['bpm']) ?
            $obj['bpm']:
            [];

        $this->date = isset($obj['date']) && is_array($obj['date']) ?
            $obj['date']:
            [];

        $this->throwback = isset($obj['throwback']) ?
            $obj['throwback']:
            null;

        $this->remix = isset($obj['remix']) ?
            $obj['remix']:
            null;

        $this->search = isset($obj['search']) && is_array($obj['search']) ?
            $obj['search']:
            [];

        $this->sort = isset($obj['sort']) && strlen($obj['sort']) > 0 ?
            $obj['sort']:
            false;
    }
}
