<?php
namespace App\Http\Controllers;

class ContentController extends Controller
{
    public function __construct()
    {
        //
    }

    public function getFAQContent()
    {
        $faq = current(
            app('db')->select("SELECT title, content FROM latenight.pages WHERE link = 'faq' LIMIT 1")
        );

        return response()->json([
            'success' => true,
            'title' => $faq->title,
            'content' => $faq->content
        ]);
    }

    public function getLandingContent() {
        $landingTitle = current(app('db')->select("SELECT `value` FROM settings WHERE `name` = 'landing_title'"));
        $landingMessage = current(app('db')->select("SELECT `text` FROM settings WHERE `name` = 'landing_message'"));
        $landingEnabled = current(app('db')->select("SELECT `enabled` FROM settings WHERE `name` = 'landing_enabled'"));
        $crateqEnabled = current(app('db')->select("SELECT `enabled` FROM settings WHERE `name` = 'crateq_enabled'"));
        return response()->json([
            'success' => true,
            'title' => $landingTitle->value,
            'message' => $landingMessage->text,
            'enabled' => $landingEnabled->enabled,
            'qenabled' => $crateqEnabled->enabled
        ]);
    }
}
