<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GenresController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getAllGenres()
    {
        $genres = app('db')
                    ->select(
                        'SELECT g.id, g.name, COUNT(*) AS `releases` '.
                        'FROM latenight.songs s '.
                        'INNER JOIN latenight.songs_genres g ON s.genre_id = g.id '.
                        'GROUP BY g.id '.
                        'ORDER BY g.name ASC'
                    );

        foreach ($genres as &$g) {
            $g->releases = number_format($g->releases);

            $g->subgenres =
                app('db')
                    ->select(
                        'SELECT g.id, g.name, COUNT(*) AS `releases` '.
                        'FROM latenight.songs s '.
                        'INNER JOIN latenight.songs_genres_sub g ON s.subgenre_id = g.id '.
                        "WHERE g.parent = {$g->id} ".
                        'GROUP BY g.id '.
                        'ORDER BY g.name ASC'
                    );

            foreach ($g->subgenres as &$sg) {
                $sg->releases = number_format($sg->releases);
            }
        }

        return response()->json($genres);
    }

    public function getAllSubgenres(Request $request)
    {
        $rank = $request->input('rank');

        $genres = app('db')
                    ->select(
                        'SELECT g.id, g.name, COUNT(*) AS `releases` '.
                        'FROM latenight.songs s '.
                        'INNER JOIN latenight.songs_genres g ON s.genre_id = g.id '.
                        'GROUP BY g.id '.
                        'ORDER BY g.name ASC'
                    );

        foreach ($genres as &$g) {
            $g->releases = number_format($g->releases);

            $subgenre_query =
                'SELECT g.id, g.name, COUNT(*) AS `releases` '.
                'FROM latenight.songs s '.
                'INNER JOIN latenight.songs_genres_sub g ON s.subgenre_id = g.id '.
                "WHERE g.parent = {$g->id} ".
                'GROUP BY g.id ';

            if ($rank === 'releases') {
                $subgenre_query .= 'ORDER BY `releases` DESC';
            } else {
                $subgenre_query .= 'ORDER BY g.name ASC';
            }

            $g->subgenres = app('db')->select($subgenre_query);

            foreach ($g->subgenres as &$sg) {
                $sg->releases = number_format($sg->releases);
            }
        }

        return response()->json($genres);
    }

    public function getAllParentGenres(Request $request)
    {
        $rank = $request->input('rank');

        $query = '';

        if ($rank === null) {
            $query = <<<HEREDOC
SELECT g.id, g.name, COUNT(*) AS `releases`
FROM latenight.songs s
INNER JOIN latenight.songs_genres g ON s.genre_id = g.id
GROUP BY g.id
ORDER BY g.name ASC
HEREDOC;

        } else if ($rank === 'releases') {
            $query = <<<HEREDOC
SELECT g.id, g.name, COUNT(*) AS `releases`
FROM latenight.songs s
INNER JOIN latenight.songs_genres g ON s.genre_id = g.id
GROUP BY g.id
ORDER BY `releases` DESC
HEREDOC;

        }

        $genres = app('db')->select($query);

        // Number format the release count
        foreach ($genres as &$g) {
            $g->releases = number_format($g->releases);
        }

        return response()->json($genres);
    }

    public function getDecades()
    {
        $decade_data = array();

        $decades =
            array(
                1970 => array(
                    'canonical' => "1970's",
                    'minimum' => 1970,
                    'maximum' => 1979
                ),
                1980 => array(
                    'canonical' => "1980's",
                    'minimum' => 1980,
                    'maximum' => 1989
                ),
                1990 => array(
                    'canonical' => "1990's",
                    'minimum' => 1990,
                    'maximum' => 1999
                ),
                2000 => array(
                    'canonical' => "2000's",
                    'minimum' => 2000,
                    'maximum' => 2009
                ),
                2010 => array(
                    'canonical' => "2010's",
                    'minimum' => 2010,
                    'maximum' => 2019
                )
            );

        foreach ($decades as $key => $info) {
            $genres = app('db')
                        ->select(
                            'SELECT COUNT(*) AS `releases` '.
                            'FROM latenight.songs s '.
                            "WHERE year BETWEEN {$info['minimum']} AND {$info['maximum']}"
                        );

            $data = array(
                'year' => $key,
                'name' => $info['canonical'],
                'releases' => number_format(current($genres)->releases)
            );

            array_push($decade_data, $data);

            unset($data);
        }

        return response()->json($decade_data);
    }

    public function getThrowbacks()
    {
        $throwback_data = array();

        $all_genres = app('db')->select("SELECT * FROM songs_genres ORDER BY id");

        foreach ($all_genres as $g) {
            $release_count = app('db')
                                ->select(
                                    'SELECT COUNT(*) AS `total` '.
                                    'FROM latenight.songs s '.
                                    'WHERE throwback = 1 '.
                                    "AND genre_id = {$g->id} "
                                );

            if (current($release_count)->total == 0) {
                continue;
            }

            array_push($throwback_data, array(
                'id' => $g->id,
                'name' => $g->name,
                'releases' => current($release_count)->total
            ));
        }

        usort($throwback_data, array($this, 'sortByTotal'));

        $throwback_data = array_map(function($r) {
            $r['releases'] = number_format($r['releases']);

            return $r;

        }, $throwback_data);

        return response()->json($throwback_data);
    }

    private function sortByTotal($a, $b)
    {
        if ($a['releases'] == $b['releases']) {
            return 0;
        }

        return $a['releases'] > $b['releases'] ? -1 : 1;
    }
}
