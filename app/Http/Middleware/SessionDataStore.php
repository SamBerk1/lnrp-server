<?php
namespace App\Http\Middleware;

use Kunnu\Dropbox\Store\PersistentDataStoreInterface;

class SessionDataStore implements PersistentDataStoreInterface {
    /**
     * Get a value from the store
     *
     * @param  string $key Data Key
     *
     * @return string
     */
    public function get($key)
    {
        return session($key);
    }

    /**
     * Set a value in the store
     * @param string $key   Data Key
     * @param string $value Data Value
     *
     * @return void
     */
    public function set($key, $value)
    {
        session([$key => $value]);
    }

    /**
     * Clear the key from the store
     *
     * @param $key Data Key
     *
     * @return void
     */
    public function clear($key)
    {
        session([$key => null]);
    }
}
