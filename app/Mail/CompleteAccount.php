<?php
namespace App\Mail;

use Illuminate\Mail\Mailable;

class CompleteAccount extends Mailable
{
    public $values;

    public function __construct($values)
    {
        $this->values = $values;
    }

    public function build()
    {
        return $this->subject("Welcome to Late Night Record Pool!")
                    ->view('email.complete-account');
    }
}
