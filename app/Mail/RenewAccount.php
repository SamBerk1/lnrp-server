<?php
namespace App\Mail;

use Illuminate\Mail\Mailable;

class RenewAccount extends Mailable
{
    public $values;

    public function __construct($values)
    {
        $this->values = $values;
    }

    public function build()
    {
        return $this->subject("Your account renewal link")
                    ->view('email.renew-account');
    }
}
