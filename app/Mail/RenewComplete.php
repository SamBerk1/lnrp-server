<?php
namespace App\Mail;

use Illuminate\Mail\Mailable;

class RenewComplete extends Mailable
{
    public $values;

    public function __construct($values)
    {
        $this->values = $values;
    }

    public function build()
    {
        return $this->subject("Your account has been renewed!")
                    ->view('email.renew-complete');
    }
}
