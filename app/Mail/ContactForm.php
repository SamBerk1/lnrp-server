<?php
namespace App\Mail;

use Illuminate\Mail\Mailable;

class ContactForm extends Mailable
{
    public $values;

    public function __construct($values)
    {
        $this->values = $values;
    }

    public function build()
    {
        return $this->subject("A {$this->values['purpose']} Contact Form has been sent!")
                    ->view('email.contact-form');
    }
}
