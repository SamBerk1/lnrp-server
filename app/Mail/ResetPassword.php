<?php
namespace App\Mail;

use Illuminate\Mail\Mailable;

class ResetPassword extends Mailable
{
    public $values;

    public function __construct($values)
    {
        $this->values = $values;
    }

    public function build()
    {
        return $this->subject("Your password reset link")
                    ->view('email.password-reset');
    }
}
