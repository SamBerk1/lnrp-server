<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetBilling extends Model
{

    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'set_billing';

    /**
     * The field considered to be the primary key
     *
     * @var bool
     */
    public $primaryKey = 'id';

    /**
     * Enable/Disable primary incrementing
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
