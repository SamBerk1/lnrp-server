<?php
namespace App\Console\Commands;

use \Illuminate\Console\Command;
use \Intervention\Image\ImageManager;

class FrontpageBackgroundCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'images:frontpage-background';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new background image for the "Always Something New" background.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $imageManager = new ImageManager(['driver' => 'gd']);

        $image = $imageManager->canvas(750, 375);

        $releases = app('db')->select("SELECT image, zip FROM latenight.songs WHERE image IS NOT NULL ORDER BY `year` DESC, `score` DESC LIMIT 0, 50");

        for ($i = 0; $i < 50; $i++) {
            $x = 75 * ($i % 10);
            $y = 75 * (floor($i / 10));

            $folder = substr($releases[$i]->zip, 0, strrpos($releases[$i]->zip, '/'));

            try {
                $sourceImage = $imageManager->make("{$folder}/{$releases[$i]->image}")->fit(75, 75);

            } catch (\Intervention\Image\Exception\NotReadableException $e) {
                $this->error("Invalid image filename found at: {$folder}/{$releases[$i]->image}");
                continue;
            }

            $image->insert($sourceImage, 'top-left', $x, $y);
        }

        $image->save(storage_path() . '/app/bg-front-new.jpg', 60);
    }

}
