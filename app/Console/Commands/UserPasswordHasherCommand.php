<?php
namespace App\Console\Commands;

use \Illuminate\Console\Command;
use \Illuminate\Support\Facades\Hash;
use \App\User;

class Cipher {
    private $securekey, $iv;

    function __construct($textkey) {
        $this->securekey = md5($textkey, TRUE);
        $this->iv = mcrypt_create_iv(32, MCRYPT_DEV_URANDOM);
    }

    function encrypt($input) {
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->securekey, $input, MCRYPT_MODE_ECB, $this->iv));
    }

    function decrypt($input) {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->securekey, base64_decode($input), MCRYPT_MODE_ECB, $this->iv));
    }
}

class UserPasswordHasherCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'users:hash-passwords';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate hash-based passwords from all the users currently in the database';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {

        $cipher = new Cipher('Late Night Record Pool');

        $users = \App\User::all();

        $bar = $this->output->createProgressBar(count($users));

        foreach ($users as $user) {
            $user->password = Hash::make($cipher->decrypt($user->old_password));
            $user->save();

            $bar->advance();
        }

        $bar->finish();

        $this->info('Passwords updated!');
    }

}
