<?php

return [
    /*
     |--------------------------------------------------------------------------
     | Laravel CORS
     |--------------------------------------------------------------------------
     |
     | allowedOrigins, allowedHeaders and allowedMethods can be set to array('*')
     | to accept any value.
     |

    'supportsCredentials' => false,
    'allowedOrigins' => ['https://latenightrecordpool.com','https://new.latenightrecordpool.com','https://www.latenightrecordpool.com'],
    'allowedHeaders' => ['*'],
    'allowedMethods' => ['*'],
    'exposedHeaders' => [],
    'maxAge' => 0,
     */

    'supportsCredentials' => false,
    'allowedOrigins' => ['https://latenightrecordpool.com','https://qa.latenightrecordpool.com','https://www.latenightrecordpool.com', 'http://localhost:51538'],
    'allowedHeaders' => ['*'],
    'allowedMethods' => ['*'],
    'exposedHeaders' => [],
    'maxAge' => 0,
    'hosts' => ['*'],

    // 'supportsCredentials' => false,
    // 'allowedOrigins' => ['*'],
    // 'allowedHeaders' => ['*'],
    // 'allowedMethods' => ['*'],
    // 'exposedHeaders' => [],
    // 'maxAge' => 0,
    // 'hosts' => ['*'],

    //  'defaults' => array(
    //     'supportsCredentials' => false,
    //     'allowedOrigins' => array('*'),
    //     'allowedHeaders' => array('*'),
    //     'allowedMethods' => array('POST', 'PUT', 'GET', 'DELETE','OPTIONS'),
    //     'exposedHeaders' => array(),
    //     'maxAge' => 0,
    //     'hosts' => array(),
    // ),

    // 'paths' => array(
    //     '*' => array(
    //         'supportsCredentials' => false,
    //         'allowedOrigins' => array('*'),
    //         'allowedHeaders' => array('*'),
    //         'allowedMethods' => array('POST', 'PUT', 'GET', 'DELETE','OPTIONS'),
    //         'maxAge' => 0,
    //         'hosts' => array('*'),
    //     ),
    // ),
];
