<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return 'LNRP Crate Server v' .env('VERSION');
});

Route::get('/download/{code}/{type?}', ['uses' => 'DownloadController@downloadFile']);

Route::group(['prefix' => '/dropbox'], function() {
    Route::get('/connect/{user?}', ['uses' => 'DropboxController@connect']);
    Route::get('/authorize', ['uses' => 'DropboxController@auth']);
    Route::get('/test/{user?}', ['uses' => 'DropboxController@test']);
});
