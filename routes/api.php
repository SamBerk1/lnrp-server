<?php

use Illuminate\Http\Request;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::options('{all:.*}', ['middleware' => 'cors', function() { return response(''); }]);

Route::group(['prefix' => '/user'], function() {
    Route::post('/login', ['uses' => 'AuthController@doLogin']);

    Route::get('/image/{id}/{width?}/{height?}', ['uses' => 'UserController@getImage']);

    Route::post('/', ['uses' => 'UserController@createAccount']);
    Route::post('/new', ['uses' => 'UserController@createNewAccount']);
    Route::put('/complete/{id}', ['uses' => 'UserController@completeAccount']);
    Route::put('/card/{id}', ['uses' => 'UserController@updateCard']);
    Route::put('/{id?}', ['uses' => 'UserController@updateAccount']);

    Route::get('/refresh', ['uses' => 'AuthController@refreshToken']);

    Route::post('/reset', ['uses' => 'UserController@sendResetLink']);
    Route::post('/reset/password', ['uses' => 'UserController@resetPassword']);

    Route::post('/renew', ['uses' => 'UserController@sendRenewLink']);
    Route::put('/renew/{code}', ['uses' => 'UserController@renewUser']);

    Route::post('/unlink', ['uses' => 'UserController@unlinkDropboxAccount']);

    Route::post('/ip', ['uses' => 'UserController@updateIPaddress']);

    Route::post('/promo', ['uses' => 'UserController@promoValidation']);
});

Route::group(['prefix' => '/dropbox'], function() {
    Route::post('/download/{release}/{version?}', ['uses' => 'DropboxController@requestDownload']);
});

Route::group(['prefix' => '/content'], function() {
    Route::get('/faq', ['uses' => 'ContentController@getFAQContent']);
    Route::get('/landing', ['uses' => 'ContentController@getLandingContent']);
});

Route::group(['prefix' => '/contact'], function() {
    Route::post('/', ['uses' => 'ContactController@sendContactForm']);
});

Route::group(['prefix' => '/braintree'], function() {
    Route::get('/client-code', ['uses' => 'BraintreeController@generateClientCode']);
});

Route::post('/download/request/{release}/{version?}', ['uses' => 'DownloadController@requestDownload']);
Route::match(['GET', 'POST'], '/download/crateq/{release}/{version?}', 'DownloadController@requestCrateQ');
Route::match(['GET', 'POST'], '/download/{version}', 'DownloadController@requestCrateQDownload');
Route::match(['GET', 'POST'], '/download/validation/{version}', 'DownloadController@requestDownloadValidation');

Route::get('/images/hero', ['uses' => 'ImagesController@getInitialHeroImages']);
Route::get('/images/hero/full', ['uses' => 'ImagesController@getFullHeroImages']);
Route::get('/images/top', ['uses' => 'ImagesController@getTopDownloadImages']);
Route::post('/images/upload', ['uses' => 'ImagesController@uploadUserImage']);
Route::get('/images/src/{id}/{width?}/{height?}/{quality?}', ['uses' => 'ImagesController@getImageByID']);

Route::get('/images/bg/front-new', ['uses' => 'ImagesController@getNewMusicBackgorund']);

Route::get('/releases/count', ['uses' => 'ReleasesController@getAllReleasesCount']);
Route::get('/releases/current/{limit}', ['uses' => 'ReleasesController@getCurrentlyPlaying']);
Route::get('/releases/months', ['uses' => 'ReleasesController@getReleaseMonths']);
Route::get('/releases/new/{month?}/{year?}', ['uses' => 'ReleasesController@getNewReleases']);
Route::post('/releases/{start}/{limit}', ['uses' => 'ReleasesController@getReleases']);

Route::get('/release/top', ['uses' => 'ReleasesController@getTopDownloadedRelease']);
Route::get('/release/detail/{id}', ['uses' => 'ReleasesController@getReleaseDetail']);
Route::get('/release/info/{id}', ['uses' => 'ReleasesController@getReleaseInfo']);
Route::get('/release/play/{id}', ['uses' => 'ReleasesController@playRelease']);
Route::get('/release/wave/{id}/{width?}/{height?}/{quality?}', ['uses' => 'ReleasesController@waveRelease']);
Route::post('/release/rate/{id}/{rating}', ['uses' => 'ReleasesController@rateRelease']);

Route::get('/version/info/{id}', ['uses' => 'ReleasesController@getVersionInfo']);
Route::get('/version/play/{id}', ['uses' => 'ReleasesController@playVersion']);
Route::get('/version/testplay/{id}', ['uses' => 'ReleasesController@testPlayVersion']);
Route::get('/version/wave/{id}/{width?}/{height?}/{quality?}', ['uses' => 'ReleasesController@waveVersion']);

Route::get('/genres', ['uses' => 'GenresController@getAllGenres']);
Route::get('/genres/subgenres', ['uses' => 'GenresController@getAllSubgenres']);
Route::get('/genres/parents', ['uses' => 'GenresController@getAllParentGenres']);
Route::get('/genres/decades', ['uses' => 'GenresController@getDecades']);
Route::get('/genres/throwbacks', ['uses' => 'GenresController@getThrowbacks']);

Route::get('/labels', ['uses' => 'LabelsController@getAllLabels']);

Route::get('/top/{genre?}/{subgenre?}', ['uses' => 'ReleasesController@getTopDownloads']);
Route::get('/recent/{limit?}', ['uses' => 'ReleasesController@getWeekTopDownloads']);

Route::get('/generate-token', ['uses' => 'TokenController@create']);
Route::get('/test-token', ['uses' => 'TokenController@test']);

Route::get('/test401', function() {
    return response('Unauthorized access', 401);
});

Route::post('/Auth/Login', ['uses' => 'CrateQController@login']);
Route::get('/Content/Genres', ['uses' => 'CrateQController@getGenres']);
Route::match(['GET', 'POST'], '/Content/Media', 'CrateQController@getReleases');
Route::match(['GET', 'POST'], '/Content/CrateQ', 'CrateQController@getCrateQ');
Route::match(['GET', 'POST'], '/Content/Preview/{id}', 'CrateQController@playPreview');
Route::match(['GET', 'POST'], '/Content/Recent-download', 'CrateQController@recentDownload');
Route::match(['GET', 'POST'], '/Content/CrateQ/{id}', 'CrateQController@getOneSong');
Route::match(['GET', 'POST'], '/Content/crateq-download-list', 'CrateQController@getCrateQDownloadList');
Route::match(['GET', 'POST'], '/Content/crateq-download/{id}', 'CrateQController@getCrateQDownload');
Route::get('/crateq/download/{type?}', ['uses' => 'CrateQController@downloadCrateQ']);
